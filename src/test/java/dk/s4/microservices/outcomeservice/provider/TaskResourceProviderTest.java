package dk.s4.microservices.outcomeservice.provider;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.outcomeservice.servlet.OutcomeServiceTest;
import dk.s4.microservices.outcomeservice.servlet.ServerFacade;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Task;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class TaskResourceProviderTest {

    private static IGenericClient ourClient;
    private static Server ourServer;
    private static String ourServerBase;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @BeforeClass
    public static void beforeClass() throws Exception {
        String value = System.getenv("DATABASE_TYPE");
        if (value == null)
            environmentVariables.set("DATABASE_TYPE", "Derby");


        //Read and set environment variables from .env file:
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);
        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml", "target/service", environmentVariables, ourPort, ourServer);

        ourClient = ServerFacade.getServerFhirContext().newRestfulGenericClient(ourServerBase);
        ourClient.registerInterceptor(new LoggingInterceptor(true));

        //Populate database for test
        IParser jsonParser = ServerFacade.getServerFhirContext().newJsonParser();

        String resourceString;

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/Task.json");
        ServerFacade.getTaskDao().create(jsonParser.parseResource(Task.class, resourceString));

        Task task = jsonParser.parseResource(Task.class, resourceString);
        task.getCode().getCoding().get(0).setCode("review");
        task.getFocus().setType("Observation");
        ServerFacade.getTaskDao().create(task);
        task.getFocus().setType(null);
        ServerFacade.getTaskDao().create(task);

        ServerFacade.forceReindexing();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }

    @Test
    public void genericSearchNoArguments() {
        String searchUrl = ourServerBase + "/Task?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void genericSearchStatusArgument() {
        String searchUrl = ourServerBase + "/Task?status=completed";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void genericSearchWrongStatusArgument() {
        String searchUrl = ourServerBase + "/Task?status=in-progress";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }


    @Test
    public void genericSearchCodeArgument() {
        String searchUrl = ourServerBase + "/Task?code=acknowledgement";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void genericSearchWrongCodeArgument() {
        String searchUrl = ourServerBase + "/Task?code=activity";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void genericSearchFocusArgument() {
        String searchUrl = ourServerBase + "/Task?focus=urn:ietf:rfc:3986|d55bdd7b-dde4-45d4-acba-157706b9e28d";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void genericSearchWrongFocusArgument() {
        String searchUrl = ourServerBase + "/Task?focus=urn:ietf:rfc:3986|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }


    @Test
    public void genericSearchSubjectArgument() {
        String searchUrl = ourServerBase + "/Task?subject=urn:oid:1.2.208.176.1.2|2512489996";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void genericSearchWrongSubjectArgument() {
        String searchUrl = ourServerBase + "/Task?subject=urn:oid:1.2.208.176.1.2|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void genericSearchOwnerArgument() {
        String searchUrl = ourServerBase + "/Task?owner=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void genericSearchWrongOwnerArgument() {
        String searchUrl = ourServerBase + "/Task?owner=urn:oid:1.2.208.176.1.1|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void genericSearchBasedOnArgument() {
        String searchUrl = ourServerBase + "/Task?based-on=urn:ietf:rfc:3986|de67e5c1-494a-41b9-924d-4c613b076a73";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void genericSearchWrongBasedOnArgument() {
        String searchUrl = ourServerBase + "/Task?based-on=urn:ietf:rfc:3986|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void genericSearchReasonCodeArgument() {
        String searchUrl = ourServerBase + "/Task?reason-code=missing-information";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void genericSearchWrongReasonCodeArgument() {
        String searchUrl = ourServerBase + "/Task?reason-code=invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void getTaskWithFocusTypeAndCodeWithOwner() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskForPatientWithCode" +
                "&identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|acknowledgement" +
                "&owner=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void getTaskWithFocusTypeAndCodeWithInvalidOwner() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskForPatientWithCode" +
                "&identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|acknowledgement" +
                "&owner=urn:oid:1.2.208.176.1.1|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }


    @Test
    public void testTaskGetAcknowledgments() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskForPatientWithCode" +
                "&identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|acknowledgement";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testTaskGetAcknowledgmentsAuthoredOn() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskForPatientWithCode" +
                "&identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|acknowledgement" +
                "&authored-on=ge2016-01-01&authored-on=le2019-01-01";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testTaskGetReview() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|review";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testTaskGetReviewForOwner() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|review" +
                "&owner=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testTaskGetReviewForInvalidOwner() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|review" +
                "&owner=urn:oid:1.2.208.176.1.1|invalidOwner";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        query.execute();
    }

    @Test
    public void testTaskGetReviewForObservations() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|review" +
                "&focusType=Observation";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testTaskGetReviewForObservationsAuthoredOn() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|review" +
                "&focusType=Observation" +
                "&authored-on=ge2016-01-01&authored-on=le2019-01-01";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testTaskGetActivity() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|activity";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);


        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testTaskGetReviewForQuestionnaireResponse() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|review" +
                "&focusType=QuestionnaireResponse";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testTaskGetReviewForObservationsAuthoredOnWrongDate() {
        String searchUrl = ourServerBase + "/Task?_query=getTaskWithFocusTypeAndCode" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|review" +
                "&focusType=Observation" +
                "&authored-on=ge2016-01-01&authored-on=le2016-02-01";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }
}

