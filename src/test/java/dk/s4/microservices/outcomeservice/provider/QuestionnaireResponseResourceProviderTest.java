package dk.s4.microservices.outcomeservice.provider;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.outcomeservice.servlet.ServerFacade;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Task;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QuestionnaireResponseResourceProviderTest {

    private static IParser jsonParser;
    private static IGenericClient ourClient;
    private static final Logger logger = LoggerFactory.getLogger(QuestionnaireResponseResourceProviderTest.class);
    private static String ourServerBase;
    private static Server ourServer;


    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testGetFocusForTasksWithCodeAndStatus() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=acknowledgement&status=completed";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetFocusForTasksWithCodeAndWrongStatus() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=acknowledgement&status=wrongStatus";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetFocusForTasksWithWrongCodeAndStatus() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=wrongCode&status=completed";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetFocusForTasksWithCodeAndStatusWithOwner() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=acknowledgement&status=completed" +
                "&owner=urn:oid:1.2.208.176.1.1|453191000016003";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetFocusForTasksWithCodeAndStatusWithWrongOwner() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=acknowledgement&status=completed" +
                "&owner=urn:oid:1.2.208.176.1.1|invalidOwner";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetFocusForTasksWithCodeAndStatusInvalidCode() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=invalid&status=completed";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetFocusForTasksWithCodeAndStatusInvalidStatus() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=acknowledgement&status=invalid";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetFocusForTasksWithCodeAndStatusWithInvalidOwner() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=acknowledgement&status=completed" +
                "&owner=urn:oid:1.2.208.176.1.1|invalid";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testSearchBySubjectIdentifier() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchBySubjectIdentifier&subject-identifier=urn:oid:1.2.208.176.1.2|2512489996";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testSearchBySubjectIdentifierWithBasedOn() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchBySubjectIdentifier&subject-identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&based-on=urn:ietf:rfc:3986|d9636b26-63b5-48ef-961e-8539c086d111";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testSearchBySubjectIdentifierWithWrongBasedOn() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchBySubjectIdentifier&subject-identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&based-on=urn:ietf:rfc:3986|wrongBasedOn";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testSearchBySubjectIdentifierWithDate() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchBySubjectIdentifier&subject-identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&authored=ge2016-01-01&authored=le2019-01-01";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testSearchBySubjectIdentifierWithInvalidDate() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchBySubjectIdentifier&subject-identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&authored=ge2016-01-01&authored=le2016-01-02";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }


    @Test
    public void testSearchBySubjectIdentifierInvalidIdentifier() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchBySubjectIdentifier&subject-identifier=urn:oid:1.2.208.176.1.2|2512489996";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testSearchByBasedOnIdentifier() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchByBasedOnIdentifier&based-on=urn:ietf:rfc:3986|d9636b26-63b5-48ef-961e-8539c086d111";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testSearchByBasedOnIdentifierWithDate() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchByBasedOnIdentifier&based-on=urn:ietf:rfc:3986|d9636b26-63b5-48ef-961e-8539c086d111" +
                "&authored=ge2016-01-01&authored=le2019-01-01";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testSearchByBasedOnIdentifierWithInvalidDate() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchByBasedOnIdentifier&based-on=urn:ietf:rfc:3986|d9636b26-63b5-48ef-961e-8539c086d111" +
                "&authored=ge2016-01-01&authored=le2016-01-02";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testSearchByBasedOnIdentifierWithInvalidIdentifier() {
        String searchUrl = ourServerBase +
                "/QuestionnaireResponse?_query=searchByBasedOnIdentifier&based-on=urn:ietf:rfc:3986|invalid";
        Bundle result = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        logger.debug(jsonParser.setPrettyPrint(true).encodeResourceToString(result));
        Assert.assertFalse(result.hasEntry());
    }


    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }


    @BeforeClass
    public static void beforeClass() throws Exception {
        String value = System.getenv("DATABASE_TYPE");
        if (value == null)
            environmentVariables.set("DATABASE_TYPE", "Derby");


        //Read and set environment variables from .env file:
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);
        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml", "target/service", environmentVariables, ourPort, ourServer);

        ourClient = ServerFacade.getServerFhirContext().newRestfulGenericClient(ourServerBase);
        ourClient.registerInterceptor(new LoggingInterceptor(true));

        //Populate database for test
        jsonParser = ServerFacade.getServerFhirContext().newJsonParser();

        String taskResourceString = ResourceUtil.stringFromResource("FHIR-clean/Task.json");
        Task task = jsonParser.parseResource(Task.class, taskResourceString);
        ServerFacade.getTaskDao().create(task);

        //String qrResourceString = ResourceUtil.stringFromResource("FHIR-clean/QuestionnaireResponse.json");
        //TODO: Use line above instead of the one below once issues around canonicals have been fixed in sample-data
        String qrResourceString = ResourceUtil.stringFromResource("QuestionnaireResponse_canonical-temp-fix.json");
        QuestionnaireResponse qr = jsonParser.parseResource(QuestionnaireResponse.class, qrResourceString);
        ServerFacade.getQrDao().create(qr);

        ServerFacade.forceReindexing();
    }
}
