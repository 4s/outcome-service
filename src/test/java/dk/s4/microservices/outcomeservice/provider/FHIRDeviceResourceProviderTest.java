package dk.s4.microservices.outcomeservice.provider;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.IQueryParameterType;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Task;
import org.junit.Before;
import org.junit.Test;

public class FHIRDeviceResourceProviderTest {
    private FHIRDeviceResourceProvider provider;
    private SearchParameterMap searchParameterMap;

    @Before
    public void setup() {
        FhirContext context = mock(FhirContext.class);
        @SuppressWarnings("unchecked")
        IFhirResourceDao<Device> deviceDao = (IFhirResourceDao<Device>) mock(IFhirResourceDao.class);
        @SuppressWarnings("unchecked")
        IFhirResourceDao<Task> taskDao = (IFhirResourceDao<Task>) mock(IFhirResourceDao.class);
        when(deviceDao.search(anyObject(), anyObject(), anyObject())).then((invocation) -> {
            searchParameterMap = (SearchParameterMap) invocation.getArguments()[0];
            return mock(IBundleProvider.class);
        });
        provider = new FHIRDeviceResourceProvider(deviceDao, context);
    }

    @Test
    public void getByIdentifierYieldExpectedSearchParameterMap(){
        TokenParam tokenParam = new TokenParam().setValue("def").setSystem("efg");
        provider.getByIdentifier(mock(HttpServletRequest.class), mock(HttpServletResponse.class), mock(RequestDetails.class), tokenParam);
        assertThat(searchParameterMap).isNotNull();
        assertThat(searchParameterMap.get("identifier")).isNotNull();
        assertThat(getTokenParamFrom(searchParameterMap.get("identifier")).getValue()).isEqualTo("def");
        assertThat(getTokenParamFrom(searchParameterMap.get("identifier")).getSystem()).isEqualTo("efg");

    }

    private TokenParam getTokenParamFrom(List<List<? extends IQueryParameterType>> list) {
        return (TokenParam) list.get(0).get(0);
    }
}