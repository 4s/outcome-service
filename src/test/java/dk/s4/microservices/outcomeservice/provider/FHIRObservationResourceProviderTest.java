package dk.s4.microservices.outcomeservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.IQueryParameterType;
import ca.uhn.fhir.parser.JsonParser;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import java.io.IOException;
import java.util.ArrayList;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.QuestionnaireResponse.QuestionnaireResponseItemAnswerComponent;
import org.hl7.fhir.r4.model.QuestionnaireResponse.QuestionnaireResponseItemComponent;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Task;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class FHIRObservationResourceProviderTest {
    private FHIRObservationResourceProvider provider;
    private SearchParameterMap taskSearchParameterMap;
    private SearchParameterMap obsSearchParameterMap;
    private IBundleProvider taskBundleProvider;
    private IBundleProvider observationBundleProvider;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();


    @Before
    public void setup() throws IOException {
        environmentVariables.set("OFFICIAL_LINKED_OBSERVATION_EXTENSION_URL",
            "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/116097031/QuestionnaireResponse+linkedObservation");

        FhirContext context = FhirContext.forR4();
        JsonParser parser = new JsonParser(context, null);
        String qrResourceString = ResourceUtil
            .stringFromResource("QuestionnaireResponse_with_observation_reference.json");
        QuestionnaireResponse qr =
            parser.parseResource(QuestionnaireResponse.class, qrResourceString);

        @SuppressWarnings("unchecked") IFhirResourceDao<Observation> observationDao =
            (IFhirResourceDao<Observation>) mock(IFhirResourceDao.class);
        @SuppressWarnings("unchecked") IFhirResourceDao<QuestionnaireResponse> qrDao =
            (IFhirResourceDao<QuestionnaireResponse>) mock(IFhirResourceDao.class);
        IBundleProvider qrBundleProvider = mock(IBundleProvider.class);

        when(qrDao.search(any(), any(), any())).thenReturn(qrBundleProvider);
        when(qrBundleProvider.size()).thenReturn(1);
        List<IBaseResource> qrs = new ArrayList<>();
        qrs.add(qr);
        when(qrBundleProvider.getResources(0, 1)).thenReturn(qrs);

        @SuppressWarnings("unchecked") IFhirResourceDao<Task> taskDao =
            (IFhirResourceDao<Task>) mock(IFhirResourceDao.class);
        taskBundleProvider = mock(IBundleProvider.class);


        when(taskBundleProvider.size()).thenReturn(0);
        when(taskDao.search(anyObject(), anyObject(), anyObject())).then(invocationOnMock -> {
            taskSearchParameterMap = (SearchParameterMap) invocationOnMock.getArguments()[0];
            return taskBundleProvider;
        });
        observationBundleProvider = mock(IBundleProvider.class);
        when(observationBundleProvider.size()).thenReturn(0);
        when(observationDao.search(anyObject(), anyObject(), anyObject())).then((invocation) -> {
            obsSearchParameterMap = (SearchParameterMap) invocation.getArguments()[0];
            return observationBundleProvider;
        });
        provider = new FHIRObservationResourceProvider(taskDao, qrDao, observationDao, context);
    }

    @Test
    public void searchForPatientWithOnlyIdYieldExpectedSearchParameterMap() {
        TokenParam tokenParam = new TokenParam().setValue("abc");
        provider.searchBySubjectIdentifier(mock(HttpServletRequest.class),
            mock(HttpServletResponse.class), mock(RequestDetails.class), tokenParam, null, null,
            null, null);
        assertThat(obsSearchParameterMap).isNotNull();
        assertThat(obsSearchParameterMap.get("subject-identifier")).isNotNull();
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("subject-identifier")).getValue())
            .isEqualTo("abc");
        assertThat(obsSearchParameterMap.get("date")).isNull();
        assertThat(obsSearchParameterMap.get("code")).isNull();
        assertThat(obsSearchParameterMap.get("basedOn-identifier")).isNull();
        assertThat(obsSearchParameterMap.get("derivedFrom-identifier")).isNull();
    }

    @Test
    public void searchForBasedOnYieldsExpectedSearchParameters() {
        TokenParam tokenParam = new TokenParam().setValue("abc");
        provider.searchByBasedOnIdentifier(mock(HttpServletRequest.class),
            mock(HttpServletResponse.class), mock(RequestDetails.class), tokenParam);
        assertThat(obsSearchParameterMap).isNotNull();
        assertThat(obsSearchParameterMap.get("basedOn-identifier")).isNotNull();
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("basedOn-identifier")).getValue())
            .isEqualTo("abc");
    }

    @Test
    public void searchForPatientWithDateAndCodeAndBasedOnYieldExpectedSearchParameterMap() {
        DateRangeParam date = new DateRangeParam();
        date.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        TokenParam code = new TokenParam().setValue("def");
        TokenParam basedOn = new TokenParam().setValue("ghi");
        TokenParam derivedFrom = new TokenParam().setValue("fisk");
        provider.searchBySubjectIdentifier(mock(HttpServletRequest.class),
            mock(HttpServletResponse.class), mock(RequestDetails.class), mock(TokenParam.class),
            date, code, basedOn, derivedFrom);
        assertThat(obsSearchParameterMap).isNotNull();
        assertThat(obsSearchParameterMap.get("subject-identifier")).isNotNull();
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("code")).getValue())
            .isEqualTo("def");
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("basedOn-identifier")).getValue())
            .isEqualTo("ghi");
        assertThat(
            getTokenParamFrom(obsSearchParameterMap.get("derivedFrom-identifier")).getValue())
            .isEqualTo("fisk");
        List<List<? extends IQueryParameterType>> dates = obsSearchParameterMap.get("date");
        assertThat(dates).hasSize(2);
        assertThat(((DateParam) dates.get(0).get(0)).getValue()).isEqualTo("1000-11-11");
        assertThat(((DateParam) dates.get(1).get(0)).getValue()).isEqualTo("1111-11-11");
    }

    @Test
    public void getByIdentifierYieldExpectedSearchParameterMap() {
        TokenParam tokenParam = new TokenParam().setValue("def").setSystem("efg");
        provider.getByIdentifier(mock(HttpServletRequest.class), mock(HttpServletResponse.class),
            mock(RequestDetails.class), tokenParam);
        assertThat(obsSearchParameterMap).isNotNull();
        assertThat(obsSearchParameterMap.get("identifier")).isNotNull();
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("identifier")).getValue())
            .isEqualTo("def");
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("identifier")).getSystem())
            .isEqualTo("efg");

    }


    @Test
    public void getFocusForTasksWithCodeAndStatusYieldExpectedSearchParameterMap() {
        TokenParam codeParam = new TokenParam().setValue("code").setSystem("codeSystem");
        TokenParam statusParam = new TokenParam().setValue("status").setSystem("statusSystem");
        TokenOrListParam ownerParam = new TokenOrListParam().add("ownerSystem", "owner");
        provider.getFocusForTasksWithCodeAndStatus(null, mock(HttpServletResponse.class),
            mock(RequestDetails.class), codeParam, statusParam, ownerParam);
        assertThat(taskSearchParameterMap).isNotNull();
        assertThat(taskSearchParameterMap.get("code")).isNotNull();
        assertThat(taskSearchParameterMap.get("status")).isNotNull();
        assertThat(taskSearchParameterMap.get("owner-identifier")).isNotNull();
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("code")).getValue())
            .isEqualTo("code");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("code")).getSystem())
            .isEqualTo("codeSystem");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("status")).getValue())
            .isEqualTo("status");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("status")).getSystem())
            .isEqualTo("statusSystem");
        assertThat(taskSearchParameterMap.get("owner-identifier").get(0).get(0)
            .getValueAsQueryToken(mock(FhirContext.class))).contains("owner");
        assertThat(taskSearchParameterMap.get("owner-identifier").get(0).get(0)
            .getValueAsQueryToken(mock(FhirContext.class))).contains("ownerSystem");

    }


    @Test
    public void getFocusForTasksWithCodeAndStatusYieldExpectedResultIfNoTasksMatch() {
        when(taskBundleProvider.size()).thenReturn(1);
        List<IBaseResource> returnList = new ArrayList<>();
        Reference reference = new Reference().setIdentifier(
            new Identifier().setValue("identifierValue").setSystem("identifierSystem"));
        reference.setType("QuestionnaireResponse");
        returnList.add(new Task().setFocus(reference));
        when(taskBundleProvider.getResources(0, 1)).thenReturn(returnList);

        TokenParam codeParam = new TokenParam().setValue("code").setSystem("codeSystem");
        TokenParam statusParam = new TokenParam().setValue("status").setSystem("statusSystem");
        TokenOrListParam ownerParam = new TokenOrListParam().add("ownerSystem", "owner");
        Bundle result = provider
            .getFocusForTasksWithCodeAndStatus(null, mock(HttpServletResponse.class),
                mock(RequestDetails.class), codeParam, statusParam, ownerParam);
        assertThat(taskSearchParameterMap).isNotNull();
        assertThat(taskSearchParameterMap.get("code")).isNotNull();
        assertThat(taskSearchParameterMap.get("status")).isNotNull();
        assertThat(taskSearchParameterMap.get("owner-identifier")).isNotNull();
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("code")).getValue())
            .isEqualTo("code");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("code")).getSystem())
            .isEqualTo("codeSystem");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("status")).getValue())
            .isEqualTo("status");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("status")).getSystem())
            .isEqualTo("statusSystem");
        assertThat(taskSearchParameterMap.get("owner-identifier").get(0).get(0)
            .getValueAsQueryToken(mock(FhirContext.class))).contains("owner");
        assertThat(taskSearchParameterMap.get("owner-identifier").get(0).get(0)
            .getValueAsQueryToken(mock(FhirContext.class))).contains("ownerSystem");

        assertThat(result.hasEntry()).isEqualTo(false);
    }

    @Test
    public void getFocusForTasksWithCodeAndStatusYieldExpectedResultIfTasksMatch() {
        when(taskBundleProvider.size()).thenReturn(1);
        List<IBaseResource> returnList = new ArrayList<>();
        Reference reference = new Reference().setIdentifier(
            new Identifier().setValue("identifierValue").setSystem("identifierSystem"));
        reference.setType("Observation");
        returnList.add(new Task().setFocus(reference));
        when(taskBundleProvider.getResources(0, 1)).thenReturn(returnList);


        TokenParam codeParam = new TokenParam().setValue("code").setSystem("codeSystem");
        TokenParam statusParam = new TokenParam().setValue("status").setSystem("statusSystem");
        TokenOrListParam ownerParam = new TokenOrListParam().add("ownerSystem", "owner");
        Bundle result = provider
            .getFocusForTasksWithCodeAndStatus(null, mock(HttpServletResponse.class),
                mock(RequestDetails.class), codeParam, statusParam, ownerParam);

        assertThat(obsSearchParameterMap).isNotNull();
        assertThat(obsSearchParameterMap.get("identifier")).isNotNull();
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("identifier")).getValue())
            .isEqualTo("identifierValue");
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("identifier")).getSystem())
            .isEqualTo("identifierSystem");

        assertThat(result.hasEntry()).isEqualTo(false);
    }

    @Test
    public void getObservationForAcknowledgementYieldsExpectedSearchParamMap() {
        when(taskBundleProvider.size()).thenReturn(1);
        TokenParam statusParam = new TokenParam().setValue("status").setSystem("statusSystem");
        TokenOrListParam ownerParam = new TokenOrListParam().add("ownerSystem", "owner");

        Bundle result = provider
            .getObservationsForAcknowledgement(null, mock(HttpServletResponse.class),
                mock(RequestDetails.class), statusParam, ownerParam);
        assertThat(taskSearchParameterMap).isNotNull();
        assertThat(taskSearchParameterMap.get("code")).isNotNull();
        assertThat(taskSearchParameterMap.get("status")).isNotNull();
        assertThat(taskSearchParameterMap.get("owner-identifier")).isNotNull();
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("code")).getValue())
            .isEqualTo("acknowledgement");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("status")).getValue())
            .isEqualTo("status");
        assertThat(getTokenParamFrom(taskSearchParameterMap.get("status")).getSystem())
            .isEqualTo("statusSystem");
        assertThat(taskSearchParameterMap.get("owner-identifier").get(0).get(0)
            .getValueAsQueryToken(mock(FhirContext.class))).contains("owner");
        assertThat(taskSearchParameterMap.get("owner-identifier").get(0).get(0)
            .getValueAsQueryToken(mock(FhirContext.class))).contains("ownerSystem");


        assertThat(obsSearchParameterMap).isNotNull();
        assertThat(obsSearchParameterMap.get("identifier")).isNotNull();
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("identifier")).getValue())
            .isEqualTo("92acac55-7b0d-4d9a-a566-14ca44e50b4c");
        assertThat(getTokenParamFrom(obsSearchParameterMap.get("identifier")).getSystem())
            .isEqualTo("urn:ietf:rfc:3986");

    }

    @Test
    public void extractObservationReferencesNoQuestionnaireResponses() {
        List<Identifier> result = provider.extractObservationReferences(new ArrayList<>());
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void extractObservationReferencesNoItems() {
        List<IBaseResource> arg = new ArrayList<>();
        arg.add(new QuestionnaireResponse());
        List<Identifier> result = provider.extractObservationReferences(arg);
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void extractObservationReferencesOneRelevantItem() {
        List<IBaseResource> arg = new ArrayList<>();
        QuestionnaireResponse response = new QuestionnaireResponse();
        response.addItem(testItemComponent(true, true, true, true, true));
        arg.add(response);
        List<Identifier> result = provider.extractObservationReferences(arg);
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void extractObservationReferencesOneRelevantItemWithinAnItem() {
        List<IBaseResource> arg = new ArrayList<>();
        QuestionnaireResponse response = new QuestionnaireResponse();
        response.addItem(new QuestionnaireResponseItemComponent()
            .addItem(testItemComponent(true, true, true, true, true)));
        arg.add(response);
        List<Identifier> result = provider.extractObservationReferences(arg);
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void extractObservationReferencesNestedItems() {
        List<IBaseResource> arg = new ArrayList<>();
        QuestionnaireResponse response = new QuestionnaireResponse();
        response.addItem(new QuestionnaireResponseItemComponent().addItem(
            new QuestionnaireResponseItemComponent().addItem(
                new QuestionnaireResponseItemComponent()
                    .addItem(testItemComponent(true, true, true, true, true)))));
        arg.add(response);
        List<Identifier> result = provider.extractObservationReferences(arg);
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void extractObservationReferencesTwoRelevantItems() {
        List<IBaseResource> arg = new ArrayList<>();
        QuestionnaireResponse response = new QuestionnaireResponse();
        response.addItem(new QuestionnaireResponseItemComponent()
            .addItem(testItemComponent(true, true, true, true, true)));
        response.addItem(testItemComponent(true, true, true, true, true));
        arg.add(response);
        List<Identifier> result = provider.extractObservationReferences(arg);
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    public void extractObservationReferencesTwoRelevantItemsOneIrrelevant() {
        List<IBaseResource> arg = new ArrayList<>();
        QuestionnaireResponse response = new QuestionnaireResponse();
        response.addItem(new QuestionnaireResponseItemComponent()
            .addItem(testItemComponent(true, true, true, true, true)));
        response.addItem(testItemComponent(true, true, true, true, true));
        response.addItem(testItemComponent(true, false, true, true, true));
        arg.add(response);
        List<Identifier> result = provider.extractObservationReferences(arg);
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    public void extractObservationReferencesFromItemNoAnswer() {
        QuestionnaireResponseItemComponent arg = testItemComponent(false, true, true, true, true);
        List<Identifier> result = provider.extractObservationReferencesFromItem(arg);
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void extractObservationReferencesFromItemNotQuantityAnswer() {
        QuestionnaireResponseItemComponent arg = testItemComponent(true, false, true, true, true);
        List<Identifier> result = provider.extractObservationReferencesFromItem(arg);
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void extractObservationReferencesFromItemNoExtension() {
        QuestionnaireResponseItemComponent arg = testItemComponent(true, true, false, true, true);
        List<Identifier> result = provider.extractObservationReferencesFromItem(arg);
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void extractObservationReferencesFromItemNoReference() {
        QuestionnaireResponseItemComponent arg = testItemComponent(true, true, true, false, true);
        List<Identifier> result = provider.extractObservationReferencesFromItem(arg);
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void extractObservationReferencesFromItemNoIdentifier() {
        QuestionnaireResponseItemComponent arg = testItemComponent(true, true, true, true, false);
        List<Identifier> result = provider.extractObservationReferencesFromItem(arg);
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void extractObservationReferencesFromItemOneIdentifier() {
        QuestionnaireResponseItemComponent arg = testItemComponent(true, true, true, true, true);
        List<Identifier> result = provider.extractObservationReferencesFromItem(arg);
        assertThat(result.size()).isEqualTo(1);
    }

    private QuestionnaireResponseItemComponent testItemComponent(boolean addAnswer,
        boolean isQuantityValue, boolean addExtension, boolean addReference,
        boolean addIdentifier) {
        Identifier identifier =
            new Identifier().setSystem("identifierSystem").setValue("identifierValue");

        Reference reference = new Reference();
        if (addIdentifier) {
            reference.setIdentifier(identifier);
        }

        Extension extension = new Extension();
        extension.setUrl(System.getenv("OFFICIAL_LINKED_OBSERVATION_EXTENSION_URL"));
        if (addReference) {
            extension.setValue(reference);
        }
        QuestionnaireResponseItemAnswerComponent answer =
            new QuestionnaireResponseItemAnswerComponent();
        if (isQuantityValue) {
            answer.setValue(new Quantity());
        } else {
            answer.setValue(new BooleanType());
        }
        if (addExtension) {
            answer.addExtension(extension);
        }
        QuestionnaireResponseItemComponent component = new QuestionnaireResponseItemComponent();
        if (addAnswer) {
            component.addAnswer(answer);
        }

        return component;
    }

    private TokenParam getTokenParamFrom(List<List<? extends IQueryParameterType>> list) {
        return (TokenParam) list.get(0).get(0);
    }
}
