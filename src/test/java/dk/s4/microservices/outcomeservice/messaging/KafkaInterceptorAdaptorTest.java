package dk.s4.microservices.outcomeservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.TestUtils;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class KafkaInterceptorAdaptorTest {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(KafkaInterceptorAdaptorTest.class);

    private KafkaInterceptorAdaptor interceptorAdaptor;
    private IParser parser;

    @Before
    public void setup() throws IOException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);


        FhirContext context = mock(FhirContext.class);
        interceptorAdaptor = new KafkaInterceptorAdaptor(context);

        parser = mock(IParser.class);
        when(context.newJsonParser()).thenReturn(parser);
    }

    @Test
    public void incomingMessagePreProcessMessageDoesNotStopObservation() {
        logger.trace("shouldProcessResourceDoesProcessQuestionnaireResponseTask");
        when(parser.parseResource((String) anyObject())).thenReturn(aTaskWithFocusType("Observation"));

        assertTrue(interceptorAdaptor.incomingMessagePreProcessMessage(aTopicWithDataType("QuestionnaireResponse"),new Message()));

    }

    @Test
    public void incomingMessagePreProcessMessageDoesNotStopQuestionnaireResponse() {
        logger.trace("incomingMessagePreProcessMessageDoesNotStopsQuestionnaireResponse");
        when(parser.parseResource((String) anyObject())).thenReturn(aTaskWithFocusType("QuestionnaireResponse"));

        assertTrue(interceptorAdaptor.incomingMessagePreProcessMessage(aTopicWithDataType("QuestionnaireResponse"),new Message()));
    }

    @Test
    public void incomingMessagePreProcessMessageStopsQuestionnaire() {
        logger.trace("incomingMessagePreProcessMessageStopsQuestionnaire");
        when(parser.parseResource((String) anyObject())).thenReturn(aTaskWithFocusType("Questionnaire"));

        assertFalse(interceptorAdaptor.incomingMessagePreProcessMessage(aTopicWithDataType("Questionnaire"),new Message()));
    }
    @Test
    public void incomingMessagePreProcessMessageStopsObservationDefinition() {
        logger.trace("incomingMessagePreProcessMessageDoesNotStopsObservationDefinition");
        when(parser.parseResource((String) anyObject())).thenReturn(aTaskWithFocusType("ObservationDefinition"));

        assertFalse(interceptorAdaptor.incomingMessagePreProcessMessage(aTopicWithDataType("ObservationDefinition"),new Message()));
    }

    @Test
    public void incomingMessagePreProcessMessageDoesNotStopOtherTopics() {
        logger.trace("incomingMessagePreProcessMessageDoesNotStopOtherTopics");
        assertTrue(interceptorAdaptor.incomingMessagePreProcessMessage(new Topic().setDataType("Observation"),new Message()));
    }


    private Task aTaskWithFocusType(String type) {
        return new Task().setFocus(new Reference().setType(type));
    }

    private Topic aTopicWithDataType(String type) {
        return new Topic().setDataType(aTaskWithFocusType(type).fhirType());
    }
}