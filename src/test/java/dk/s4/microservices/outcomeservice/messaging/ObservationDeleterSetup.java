package dk.s4.microservices.outcomeservice.messaging;

public class ObservationDeleterSetup {

    /**
     * For testing
     */
    public ObservationDeleter getObservationDeleter(MyEventProcessor eventProcessor) {
        return eventProcessor.getObservationDeleter();
    }

    /**
     * For testing
     */
    public void setObservationDeleter(MyEventProcessor eventProcessor,ObservationDeleter observationDeleter) {
        eventProcessor.setObservationDeleter(observationDeleter);
    }
}
