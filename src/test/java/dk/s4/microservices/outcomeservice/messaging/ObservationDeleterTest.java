package dk.s4.microservices.outcomeservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.jpa.searchparam.registry.ISearchParamRegistry;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import dk.s4.microservices.microservicecommon.fhir.SearchParameterFacade;
import dk.s4.microservices.outcomeservice.TestFhirServerConfigR4;
import dk.s4.microservices.outcomeservice.Utils.CarePlanRetriever;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static dk.s4.microservices.microservicecommon.fhir.ResourceUtil.getResourceStream;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestFhirServerConfigR4.class})
public class ObservationDeleterTest {

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Autowired
    @Qualifier("myObservationDaoR4")
    private IFhirResourceDao<Observation> observationDao;

    @Autowired
    private DaoConfig daoConfig;

    @Autowired
    @Qualifier("mySearchParameterDaoR4")
    private IFhirResourceDao<SearchParameter> searchParameterDaoR4;

    @Autowired
    private ISearchParamRegistry searchParamRegistry;

    private ObservationDeleter observationDeleter;

    @Before
    public void before() throws IOException {
        FhirContext fhirContext =  FhirContext.forR4();
        SearchParameterFacade searchParameterFacade = new SearchParameterFacade(searchParameterDaoR4, fhirContext);
        searchParameterFacade.installSearchParameter(getResourceStream("Observation_SubjectIdentifierSearchParam.json"));
        searchParamRegistry.forceRefresh();

        environmentVariables.set("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM", "urn:oid:1.2.208.176.1.2");
        this.observationDeleter = new ObservationDeleter(observationDao,daoConfig);


        // empty DB
        IBundleProvider found = observationDao.search(new SearchParameterMap());
        for(IBaseResource base : found.getResources(0, found.size())){
            observationDao.delete(base.getIdElement());
        }
    }

    @Test
    public void removeConsentFromDb() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";

        observationDao.create(createObservation(deleteId));
        observationDao.create(createObservation(deleteId));
        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb910"));
        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        Future<?> future = this.observationDeleter.deleteObservationsByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = observationDao.search(new SearchParameterMap());
        Assert.assertEquals(3, found.size().intValue());

        for(IBaseResource base : found.getResources(0, found.size())){
            Observation o = (Observation) base;
            Assert.assertNotEquals(o.getSubject().getIdentifier().getValue(), deleteId);
        }
    }

    @Test
    public void removeConsentEmptyDb() throws Exception{
        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";
        Future<?> future = this.observationDeleter.deleteObservationsByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);
        IBundleProvider found = observationDao.search(new SearchParameterMap());
        Assert.assertEquals(0, found.size().intValue());
    }

    @Test
    public void removeConsentNotInDb() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";

        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb910"));
        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        Future<?> future = this.observationDeleter.deleteObservationsByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = observationDao.search(new SearchParameterMap());

        Assert.assertEquals(3, found.size().intValue());

        for(IBaseResource base : found.getResources(0, found.size())){
            Observation o = (Observation) base;
            Assert.assertNotEquals(o.getSubject().getIdentifier().getValue(), deleteId);
        }
    }

    @Test
    public void removeConsentWithWrongSystem() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb910";
        observationDao.create(createObservation(deleteId, "invalidSystem"));
        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        observationDao.create(createObservation("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        Future<?> future = this.observationDeleter.deleteObservationsByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = observationDao.search(new SearchParameterMap());

        Assert.assertEquals(3, found.size().intValue());

    }

    private Observation createObservation(String id, String system){
        Observation o = new Observation();
        o.setSubject(new Reference().setIdentifier(new Identifier().setSystem(system)
                .setValue(id)));
        return o;
    }

    private Observation createObservation(String id){
        return createObservation(id, "urn:oid:1.2.208.176.1.2");
    }
}
