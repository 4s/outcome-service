package dk.s4.microservices.outcomeservice.messaging;

import ca.uhn.fhir.jpa.dao.DaoConfig;
import dk.s4.microservices.messaging.EventProducer;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.outcomeservice.Utils.CarePlanRetriever;
import javax.servlet.http.HttpServletRequest;
import org.hl7.fhir.r4.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.ArgumentCaptor;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class MyEventProcessorTest {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MyEventProcessorTest.class);

    private UserContextResolverInterface userContextResolver;
    private EventProducer eventProducer;
    private ArgumentCaptor topicCapture;
    private ArgumentCaptor messageCapture;
    private MyEventProcessor processor;
    private ObservationDeleter observationDeleter;

    private final String OrgIdentifierValue = "ORGANIZATION_IDENTIFIER_VALUE";
    private final String OrgIdentifierSystem = "ORGANIZATION_IDENTIFIER_SYSTEM";

    @Before
    public void setup() throws IOException, UserContextResolverInterface.UserContextResolverException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        topicCapture = ArgumentCaptor.forClass(Topic.class);
        messageCapture = ArgumentCaptor.forClass(Message.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        observationDeleter = mock(ObservationDeleter.class);

        when(userContextResolver.getFHIRUserOrganization((HttpServletRequest) anyObject())).thenReturn(new Identifier().setValue(OrgIdentifierValue).setSystem(OrgIdentifierSystem));
        CarePlanRetriever carePlanRetriever = mock(CarePlanRetriever.class);
        processor = new MyEventProcessor(null,null,null,null,null,carePlanRetriever, observationDeleter);
    }

    @Test
    public void testConstructTaskYieldsExpectedTask_ObservationResource() throws UserContextResolverInterface.UserContextResolverException {
        logger.debug("testConstructTaskYieldsExpectedTask");
        Message message = new Message().setCorrelationId("correlationId")
                .setTransactionId("transactionId")
                .setSession("session");

        Observation observation = new Observation().addIdentifier(new Identifier().setValue("value"));
        Task task = processor.constructTask(observation, message);

        assertEquals("Observation", task.getFocus().getType());
    }

    @Test
    public void testConstructTaskYieldsExpectedTask_QuestionnaireResponseResource() throws UserContextResolverInterface.UserContextResolverException {
        logger.debug("testConstructTaskYieldsExpectedTask");
        Message message = new Message().setCorrelationId("correlationId")
                .setTransactionId("transactionId")
                .setSession("session");

        QuestionnaireResponse questionnaireResponse = new QuestionnaireResponse().setIdentifier(new Identifier().setValue("value"));
        Task task = processor.constructTask(questionnaireResponse, message);

        assertEquals("QuestionnaireResponse", task.getFocus().getType());
    }

    @Test
    public void testConstructTaskYieldsExpectedTask_InvalidResource() throws UserContextResolverInterface.UserContextResolverException {
        logger.debug("testConstructTaskYieldsExpectedTask");
        Message message = new Message().setCorrelationId("correlationId")
                .setTransactionId("transactionId")
                .setSession("session");

        Organization organization = new Organization().addIdentifier(new Identifier().setValue("value"));
        Task task = processor.constructTask(organization, message);

        assertNull(task.getFocus().getType());
    }

    @Test
    public void testPostToKafkaSendsExpectedMessage(){
        eventProducer = mock(EventProducer.class);
        processor.setEventProducer(eventProducer);
        doNothing().when(eventProducer).sendMessage((Topic) topicCapture.capture(), (Message) messageCapture.capture());

        logger.debug("testPostToKafka");
        Message message = new Message().setCorrelationId("correlationId")
                .setTransactionId("transactionId")
                .setSession("session");
        String resourceString = "resourceString";

        processor.postToKafka(message,resourceString,new Topic().setDataType("dataType"));

        Message sentMessage = (Message) messageCapture.getValue();

        assertEquals("correlationId", sentMessage.getCorrelationId());
        assertEquals("transactionId", sentMessage.getTransactionId());
        assertEquals("session", sentMessage.getSession());
        assertEquals("Task", sentMessage.getBodyType());
        assertEquals("resourceString", sentMessage.getBody());
    }

    @Test
    public void testPostToKafkaSendsExpectedTopic(){
        eventProducer = mock(EventProducer.class);
        processor.setEventProducer(eventProducer);
        doNothing().when(eventProducer).sendMessage((Topic) topicCapture.capture(), (Message) messageCapture.capture());

        logger.debug("testPostToKafka");
        Message message = new Message();
        String resourceString = "resourceString";

        processor.postToKafka(message,resourceString,new Topic().setDataType("dataType"));

        Topic topic = (Topic) topicCapture.getValue();

        assertEquals(topic.getDataType(), "dataType");
    }
}