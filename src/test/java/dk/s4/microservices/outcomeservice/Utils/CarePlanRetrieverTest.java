package dk.s4.microservices.outcomeservice.Utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.validation.constraints.AssertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CarePlanRetrieverTest {

    CarePlanRetriever carePlanRetriever;
    private UserContextResolverInterface userResolver;
    private MyFhirClientFactory clientFactory;
    private FhirContext fhirContext;
    private IGenericClient client;
    private IUntypedQuery untypedQuery;
    private IQuery query;
    private String url;

    @Before
    public void setup() throws IOException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        fhirContext = mock(FhirContext.class);
        clientFactory = mock(MyFhirClientFactory.class);
        client = mock(IGenericClient.class);
        untypedQuery = mock(IUntypedQuery.class);
        query = mock(IQuery.class);

        when(clientFactory.getClientFromBaseUrl(anyObject(),anyObject())).thenReturn(client);
        when(client.search()).thenReturn(untypedQuery);
        when(client.getServerBase()).thenReturn("PatientCareServiceBaseUrl");
        when(untypedQuery.byUrl(anyObject())).then((invocationOnMock) -> {
            url = (String) invocationOnMock.getArguments()[0];
            return query;
        });
        when(query.returnBundle(anyObject())).thenReturn(query);

    }

    @Test
    public void getCarePlanReferenceFromSession() {
        Bundle bundle = mock(Bundle.class);
        Bundle.BundleEntryComponent bundleComponent = mock(Bundle.BundleEntryComponent.class);
        List list = mock(List.class);
        CarePlan carePlan = new CarePlan().setAuthor(new Reference().setIdentifier(new Identifier().setValue("value").setSystem("system")))
                .setStatus(CarePlan.CarePlanStatus.ACTIVE);
        when(bundleComponent.getResource()).thenReturn(carePlan);
        when(bundle.getEntryFirstRep()).thenReturn(bundleComponent);
        when(query.execute()).thenReturn(bundle);
        when(bundle.hasEntry()).thenReturn(true);
        when(bundle.getEntry()).thenReturn(list);
        when(list.isEmpty()).thenReturn(false);


        carePlanRetriever = new CarePlanRetriever("PatientCareServiceBaseUrl", fhirContext, clientFactory);

        Reference reference = carePlanRetriever.getCarePlanAuthorFromBasedOn(new Identifier().setValue("value").setSystem("system"));

        Assert.assertNotNull(url);
        Assert.assertTrue(url.contains("/CarePlan?identifier="));
        Assert.assertTrue(url.contains("system|value"));
        Assert.assertTrue(url.contains("PatientCareServiceBaseUrl"));

        Assert.assertTrue(reference.getIdentifier().getValue().contains("value"));
        Assert.assertTrue(reference.getIdentifier().getSystem().contains("system"));
    }
}