package dk.s4.microservices.outcomeservice.Utils;

import ca.uhn.fhir.rest.api.server.IBundleProvider;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Task;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TaskBundleFilterTest {

    private static String identifierValue = "identifierValue";
    private static String identifierSystem = "identifierSystem";
    private static Identifier identifier = new Identifier().setValue(identifierValue).setSystem(identifierSystem);
    private final TaskBundleFilter taskBundleFilter = new TaskBundleFilter();
    private IBundleProvider provider;
    private ArrayList<IBaseResource> tasks;

    @Before
    public void setup() {
        provider = mock(IBundleProvider.class);
    }

    @Test
    public void testValidFocusIsNotFiltered() {
        when(provider.getResources(anyInt(),anyInt())).thenReturn(Collections.singletonList(
                aTaskWithFocus(identifier,"Observation")));

        List<Task> filteredTasks = taskBundleFilter.apply(provider);

        assertEquals(filteredTasks.size(), 1);

        assertValidTasks(filteredTasks);
    }

    @Test
    public void testFocusWithInvalidIdentifierIsFiltered() {
        when(provider.getResources(anyInt(),anyInt())).thenReturn(Collections.singletonList(
                new Task().setFocus(new Reference())));

        assertEquals(taskBundleFilter.apply(provider).size(),0);
    }

    @Test
    public void testNoFocusIsFiltered() {
        when(provider.getResources(anyInt(),anyInt())).thenReturn(Collections.singletonList(
                new Task()));

        assertEquals(taskBundleFilter.apply(provider).size(),0);
    }

    @Test
    public void testInvalidFocusTypeIsFiltered() {
        when(provider.getResources(anyInt(),anyInt())).thenReturn(Collections.singletonList(
                aTaskWithFocus(identifier, "Questionnaire")));

        assertEquals(taskBundleFilter.apply(provider).size(),0);
    }

    @Test
    public void testInvalidFocusIdentifierValueIsFiltered() {
        when(provider.getResources(anyInt(),anyInt())).thenReturn(Collections.singletonList(
                aTaskWithFocus(new Identifier(), "Observation")));

        assertEquals(taskBundleFilter.apply(provider).size(),0);
    }

    @Test
    public void testFilterSeveralTasksCorrectly() {
        when(provider.getResources(anyInt(),anyInt())).thenReturn(Arrays.asList(
                aTaskWithFocus(new Identifier(),"Observation"),
                aTaskWithFocus(identifier,"Questionnaire"),
                new Task(),
                aTaskWithFocus(identifier, "Observation")));

        List<Task> filteredTasks = taskBundleFilter.apply(provider);

        assertEquals(filteredTasks.size(), 1);

        assertValidTasks(filteredTasks);
    }


    private void assertValidTasks(List<Task> tasks){
        for (Task task : tasks) {
            assertTrue(task.hasFocus());
            assertTrue(task.getFocus().hasIdentifier());
            assertTrue(task.getFocus().hasType());
            assertEquals("Observation", task.getFocus().getType());
        }
    }

    private Task aTaskWithFocus(Identifier identifier) {
        return new Task().setFocus(new Reference().setIdentifier(identifier));
    }

    private Task aTaskWithFocus(Identifier identifier, String type) {
        Task task = aTaskWithFocus(identifier);
        task.getFocus().setType(type);
        return task;
    }
}