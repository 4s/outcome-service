package dk.s4.microservices.outcomeservice.servlet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;


import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;


public class OutcomeServiceTopicPatternTest {
    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OutcomeServiceTopicPatternTest.class);

    @Test
    public void testGetTopicPatternString(){
        logger.debug("testGetTopicPatternString : " + OutcomeService.getTopicPatternString());
        assertThat(OutcomeService.getTopicPatternString())
                .isEqualTo("InputReceived_FHIR_Bundle(_.*)*|Create_FHIR_Observation(_.*)*|Create_FHIR_QuestionnaireResponse|Create_FHIR_Device|Create_FHIR_Task|Update_FHIR_Observation(_.*)*|Update_FHIR_QuestionnaireResponse|Update_FHIR_Device|Update_FHIR_Task|Delete_System_ConsentDeleted");
    }

    @Test
    public void getTopicPatternString_RequiredEnvironmentVariableDeclared_ReturnsValueOfEnvironmentVariable() {
        environmentVariables.set("SERVICE_INPUT_TOPIC_PATTERN_STRING","InputReceived_FHIR_Bundle(_.*)*|Create_FHIR_Observation(_.*)*");
        assertEquals(OutcomeService.getTopicPatternString(),"InputReceived_FHIR_Bundle(_.*)*|Create_FHIR_Observation(_.*)*");
    }

    @Test
    public void getTopicPatternString_RequiredEnvironmentVariableIsNull_ReturnsDefaultTopicPatternString() {
        environmentVariables.set("SERVICE_INPUT_TOPIC_PATTERN_STRING",null);
        assertEquals(OutcomeService.getTopicPatternString(),"InputReceived_FHIR_Bundle(_.*)*|Create_FHIR_Observation(_.*)*|Create_FHIR_QuestionnaireResponse|Create_FHIR_Device|Create_FHIR_Task|Update_FHIR_Observation(_.*)*|Update_FHIR_QuestionnaireResponse|Update_FHIR_Device|Update_FHIR_Task|Delete_System_ConsentDeleted");
    }


    @Test
    public void getTopicPatternString_RequiredEnvironmentVariableIsEmptyString_ReturnsDefaultTopicPatternString() {
        environmentVariables.set("SERVICE_INPUT_TOPIC_PATTERN_STRING","");
        assertEquals(OutcomeService.getTopicPatternString(),"InputReceived_FHIR_Bundle(_.*)*|Create_FHIR_Observation(_.*)*|Create_FHIR_QuestionnaireResponse|Create_FHIR_Device|Create_FHIR_Task|Update_FHIR_Observation(_.*)*|Update_FHIR_QuestionnaireResponse|Update_FHIR_Device|Update_FHIR_Task|Delete_System_ConsentDeleted");
    }
}
