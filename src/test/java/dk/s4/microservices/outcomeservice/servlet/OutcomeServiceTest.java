package dk.s4.microservices.outcomeservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.jpa.util.ExpungeOptions;
import ca.uhn.fhir.jpa.util.ExpungeOutcome;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Message.Prefer;
import dk.s4.microservices.messaging.MessageParseException;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.TopicParseException;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.messaging.mock.MockKafkaConsumeAndProcess;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.outcomeservice.Utils.CarePlanRetriever;
import dk.s4.microservices.outcomeservice.Utils.ResultCaptor;
import dk.s4.microservices.outcomeservice.messaging.MyEventProcessor;
import java.io.IOException;
import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import javax.servlet.http.HttpServletRequest;

import dk.s4.microservices.outcomeservice.messaging.ObservationDeleter;
import dk.s4.microservices.outcomeservice.messaging.ObservationDeleterSetup;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.*;
import org.json.simple.parser.ParseException;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

public class OutcomeServiceTest {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OutcomeServiceTest.class);

    private static long FUTURE_TIMEOUT = 5000;
    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;
    private static IGenericClient ourClient;
	private static FhirContext ourCtx = FhirContext.forR4();
	private static int ourPort;
	private static Server ourServer;
	private static String ourServerBase;
    private static String ENVIRONMENT_FILE = "service.env";

	@ClassRule
	public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    /**
     * Before running test, clean out the database (delete jpaserver_derby_files folder).
     * It will not pass if the test has already been run.
     */
    @Test
    public void testConsumeAndProcessBundleConsolidated() throws Exception {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Q-M-links/BundleConsolidated.json");

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(resourceString);
        message.setSender("OutcomeServiceTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("Bundle");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.debug("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        List<String> dataCodes = Arrays.asList("MDC149546", "MDC188736", "MDC150020");
        Topic inputTopic = new Topic()
                .setOperation(Operation.InputReceived)
                .setDataCategory(Category.FHIR)
                .setDataType("Bundle")
                .setDataCodes(dataCodes);
        ProducerRecord<String, String> record =
                new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        assertNotNull(metadata);

        Topic outputTopic;
        Message outputMessage;
        Future<Message> future;

        //Test TransactionCompleted
        outputTopic = new Topic()
                .setOperation(Operation.TransactionCompleted)
                .setDataCategory(Category.FHIR)
                .setDataType("Bundle")
                .setDataCodes(dataCodes);

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Bundle", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());

        //Test creation of Observation with code MDC149546
        outputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType("Observation")
                .addDataCode("MDC149546");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Observation", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertTrue(outputMessage.getLocation().startsWith("Observation"));
        assertEquals("1", outputMessage.getEtagVersion());

        //Test creation of Observation with code MDC188736
        outputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType("Observation")
                .addDataCode("MDC188736");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Observation", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertTrue(outputMessage.getLocation().startsWith("Observation"));
        assertEquals("1", outputMessage.getEtagVersion());

        //Test creation of Observation with code MDC150020
        outputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType("Observation")
                .addDataCode("MDC150020");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Observation", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertTrue(outputMessage.getLocation().startsWith("Observation"));
        assertEquals("1", outputMessage.getEtagVersion());

        //Test creation of QuestionnaireResponse
        outputTopic = FhirTopics.dataCreated("QuestionnaireResponse");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("QuestionnaireResponse", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertTrue(outputMessage.getLocation().startsWith("QuestionnaireResponse"));
        assertEquals("1", outputMessage.getEtagVersion());
    }

    @Test
    public void testCreateTask() throws Exception {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Task.json");

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(resourceString);
        message.setSender("OutcomeServiceTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("Task");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.debug("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("Task");
        ProducerRecord<String, String> record =new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        assertNotNull(metadata);

        Topic outputTopic;
        Message outputMessage;
        Future<Message> future;

        //Test TransactionCompleted
        outputTopic = FhirTopics.dataCreated("Task");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);
        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertNotNull(outputMessage);
    }

    @Test
    public void testCreateIfNoneExistDevice() throws Exception {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_WeightScale.json");

        Message message = new Message().
                setSender("OutcomeServiceTest").
                setBodyCategory(Message.BodyCategory.FHIR).
                setBodyType("Device").
                setContentVersion(System.getenv("FHIR_VERSION")).
                setCorrelationId(MessagingUtils.verifyOrCreateId(null)).
                setTransactionId(UUID.randomUUID().toString()).
                setIfNoneExist("identifier=urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680|00-09-1F-FE-FF-80-4A-24").
                setBody(resourceString);

        logger.debug("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("Device");
        ProducerRecord<String, String> record =new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        assertNotNull(metadata);

        Topic outputTopic;
        Message outputMessage;
        Future<Message> future;

        outputTopic = FhirTopics.dataCreated("Device");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);
        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertNotNull(outputMessage);

        //////////////////
        //Sending same Device create msg. again - checking that it doesn't get created twice

        logger.debug("Sending message a second time");
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        RecordMetadata metadata2 = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata2.offset());

        assertNotNull(metadata2);

        Topic outputTopic2;
        Message outputMessage2;
        Future<Message> future2;

        outputTopic2 = FhirTopics.processingOk("Device");

        future2 = mockKafkaEventProducer.getTopicFuture(outputTopic2);
        outputMessage2 = future2.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertNotNull(outputMessage2);
    }

    @Test
    public void testDeleteObservation() throws Exception {

        emptyObservationDb();

        //  Insert an observation which is not to be deleted.
        Observation o2 = new Observation();
        o2.setSubject(new Reference().setIdentifier(new Identifier().setSystem("urn:oid:1.2.208.176.1.2")
                .setValue("3a3a43bf-507c-46b5-a265-c98c07bfb908")));
        OutcomeService.getObservationDao().create(o2);

        // insert observation to be deleted
        String userId = "bfe93ee3-8904-4de3-882f-d23680fe8855";
        Observation o = new Observation();
        o.setSubject(new Reference().setIdentifier(new Identifier().setSystem("urn:oid:1.2.208.176.1.2").setValue(userId)));

        IIdType forDeletion = OutcomeService.getObservationDao().create(o).getId();
        OutcomeService.getObservationDao().read(forDeletion);

        // store db size to verify that deletion will deduct 1
        IBundleProvider found = OutcomeService.getObservationDao().search(new SearchParameterMap());
        Assert.assertEquals(2, found.size().intValue());

        ObjectNode node = new ObjectMapper().createObjectNode();
        node.put("userId", userId);
        node.put("consentSpecId", "someConsentSpecId");
        node.put("timestamp", Instant.now().toString());

        Message message = new Message().
                setSender("OutcomeServiceTest").
                setBodyCategory(Message.BodyCategory.System).
                setCorrelationId(MessagingUtils.verifyOrCreateId(null)).
                setTransactionId(UUID.randomUUID().toString()).
                setBody(node.toString()).
                setContentVersion(System.getenv("FHIR_VERSION"));
        logger.debug("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Delete)
                .setDataCategory(Category.System)
                .setDataType("ConsentDeleted");

        try{
            OutcomeService.getObservationDao().read(forDeletion.withVersion("1"));
        }
        catch (ResourceNotFoundException e){
            fail();
        }

        ObservationDeleterSetup observationDeleterSetup = new ObservationDeleterSetup();
        ObservationDeleter observationDeleter = observationDeleterSetup.getObservationDeleter((MyEventProcessor) OutcomeService.getEventProcessor());
        ObservationDeleter spyObservationDeleter = Mockito.spy(observationDeleter);
        observationDeleterSetup.setObservationDeleter((MyEventProcessor) OutcomeService.getEventProcessor(),spyObservationDeleter);
        ResultCaptor<Future<?>> resultCaptor = new ResultCaptor<>();
        doAnswer(resultCaptor).when(spyObservationDeleter).deleteObservationsByUserIdentifier(userId);

        ProducerRecord<String, String> record =new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        assertNotNull(metadata);

        Topic outputTopic = new Topic()
                .setOperation(Operation.ProcessingOk)
                .setDataCategory(Category.System)
                .setDataType("ConsentDeleted");

        Future<Message> afuture = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = afuture.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(outputMessage.getBodyCategory(), Message.BodyCategory.System);
        assertEquals(outputMessage.getSender(), System.getenv("SERVICE_NAME"));
        assertEquals(outputMessage.getContentVersion(), System.getenv("FHIR_VERSION"));

        OperationOutcome outcome = (OperationOutcome) OutcomeService.getServerFhirContext().newJsonParser().parseResource(outputMessage.getBody());
        assertEquals(outcome.getIssue().size(), 1);
        assertEquals(outcome.getIssue().get(0).getSeverity(), OperationOutcome.IssueSeverity.INFORMATION);
        assertEquals(outcome.getIssue().get(0).getDetails().getText(), "Successfully deleted observations for user: " + userId);

        Future<?> deleteObservationsFuture = resultCaptor.getResult();
        deleteObservationsFuture.get(1000,TimeUnit.MILLISECONDS);

        try{
            OutcomeService.getObservationDao().read(forDeletion.withVersion("1"));
            OutcomeService.getObservationDao().read(forDeletion.withVersion("2"));
            fail();
        }
        catch (ResourceNotFoundException e){
            // good
        }

        found = OutcomeService.getObservationDao().search(new SearchParameterMap());
        assertEquals(1, found.size().intValue() );

        // verify that the observation marked for deletion is not searchable.
        for(IBaseResource ibr: found.getResources(0, found.size())){
            Observation qr = (Observation) ibr;
            assertNotEquals(userId, qr.getSubject().getIdentifier().getValue());
        }
    }

    private void emptyObservationDb(){
        IBundleProvider found = OutcomeService.getObservationDao().search(new SearchParameterMap());

        DaoConfig theDaoConfig = OutcomeService.getDaoConfig();
        boolean expungeEnabled = theDaoConfig.isExpungeEnabled();
        theDaoConfig.setExpungeEnabled(true);

        // empty the database
        for(IBaseResource ibr: found.getResources(0, found.size())){
            OutcomeService.getObservationDao().delete(ibr.getIdElement());

            ExpungeOutcome expOutcome = OutcomeService.getObservationDao().expunge(ibr.getIdElement().toUnqualifiedVersionless(), new ExpungeOptions()
                    .setExpungeDeletedResources(true)
                    .setExpungeEverything(true)
                    .setExpungeOldVersions(true));
            expOutcome.getDeletedCount();
        }

        theDaoConfig.setExpungeEnabled(expungeEnabled);

    }

    //Testing ingoing topic w. non MDC code at the end
    @Test
    public void testCreateWound() throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {
        //Read resource from file
        final String messageString = ResourceUtil.stringFromResource("createWoundKafkaMsg.json");

        Topic inputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED"+"416462003");

        logger.debug("Sending message to KafkaProducer: " + messageString);
        //Send message via KafkaProducer, and wait 10000 ms for Future to return
        ProducerRecord<String, String> record =new ProducerRecord<>(inputTopic.toString(), messageString);
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        assertNotNull(metadata);

        Topic outputTopic;
        Message outputMessage;
        Future<Message> future;

        //Test TransactionCompleted
        outputTopic = new Topic()
                .setOperation(Operation.TransactionCompleted)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED"+"416462003");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);
        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertNotNull(outputMessage);
        assertEquals("Bundle", outputMessage.getBodyType());
    }

    //Testing ingoing topic w. non MDC code at the end
    @Test
    public void testCreateWoundRegistrationDetail()
            throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException, TopicParseException {
        //Read resource from file
        final String messageString = ResourceUtil.stringFromResource("createWoundRegistrationDetailKafkaMsg.json");

        Topic inputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED401191002")
                .addDataCode("SNOMED405161002")
                .addDataCode("SNOMED298007001")
                .addDataCode("HD360wound-inflammation-code")
                .addDataCode("HD360activity-level-code")
                .addDataCode("HD360wound-size-code");

        logger.debug("Sending message to KafkaProducer: " + messageString);
        //Send message via KafkaProducer, and wait 10000 ms for Future to return
        ProducerRecord<String, String> record =new ProducerRecord<>(inputTopic.toString(), messageString);
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        assertNotNull(metadata);

        Topic outputTopic;
        Message outputMessage;
        Future<Message> future;

        //Test TransactionCompleted
        outputTopic = new Topic()
                .setOperation(Operation.TransactionCompleted)
                .setDataCategory(Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED401191002")
                .addDataCode("SNOMED405161002")
                .addDataCode("SNOMED298007001")
                .addDataCode("HD360wound-inflammation-code")
                .addDataCode("HD360activity-level-code")
                .addDataCode("HD360wound-size-code");

        future = mockKafkaEventProducer.getTopicFuture(outputTopic);
        outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertNotNull(outputMessage);
        assertEquals("Bundle", outputMessage.getBodyType());
    }

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        //Use Derby local file based database
        String value = System.getenv("DATABASE_TYPE");
        if (value == null)
            environmentVariables.set("DATABASE_TYPE", "Derby");

        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = OutcomeServiceTest.class.getClassLoader().getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        logger.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        TestUtils.readEnvironment(path + "/" + ENVIRONMENT_FILE, environmentVariables);
        TestUtils.readEnvironment(path + "/deploy.env", environmentVariables);
        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer(path+"/src/main/webapp/WEB-INF/without-keycloak/web.xml",path+"/target/service", environmentVariables, ourPort, ourServer);

        // Instantiate mock Kafka classes
        mockExternalKafkaProducer = TestUtils.initMockExternalProducer();
        initMockKafkaConsumeAndProcess();

        //Init RESTful client
        initClient();
    }

    private static void initMockKafkaConsumeAndProcess() throws KafkaInitializationException, MessagingInitializationException, UserContextResolverInterface.UserContextResolverException {
        UserContextResolverInterface userContextResolver = mock(UserContextResolverInterface.class);
        when(userContextResolver.getFHIRUserOrganization((HttpServletRequest) anyObject())).thenReturn(new Identifier().setValue("OrganizationIdentifierValue"));
        CarePlanRetriever carePlanRetriever = mock(CarePlanRetriever.class);
        ObservationDeleter observationDeleter = new ObservationDeleter(OutcomeService.getObservationDao(),OutcomeService.getDaoConfig());
        OutcomeService.setEventProcessor(new MyEventProcessor(
                OutcomeService.getServerFhirContext(),
                OutcomeService.getObservationDao(),
                OutcomeService.getDeviceDao(),
                OutcomeService.getQrDao(),
                OutcomeService.getTaskDao(),
                carePlanRetriever,
                observationDeleter
                ));
        mockKafkaEventProducer = new MockKafkaEventProducer(System.getenv("SERVICE_NAME"));
        ((MyEventProcessor)OutcomeService.getEventProcessor()).setEventProducer(mockKafkaEventProducer);

        MockKafkaConsumeAndProcess kafkaConsumeAndProcess = new MockKafkaConsumeAndProcess(
                OutcomeService.getTopicPattern(),
                mockKafkaEventProducer,
                OutcomeService.getEventProcessor());
        Thread mockKafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);
        mockKafkaConsumeAndProcessThread.start();
    }

    private static void initClient() {
        ourClient = ourCtx.newRestfulGenericClient(ourServerBase);
        ourClient.registerInterceptor(new LoggingInterceptor(true));
    }
}
