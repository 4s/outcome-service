package dk.s4.microservices.outcomeservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import dk.s4.microservices.messaging.EventProcessor;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Task;

import java.util.regex.Pattern;

/**
 * Facade for accessing thing on the Service.
 */
public class  ServerFacade {
    private ServerFacade() {}

    public static void forceReindexing() {
        OutcomeService.forceReindexing();
    }

    public static IFhirResourceDao<Observation> getObservationDao() {
        return OutcomeService.getObservationDao();
    }

    public static IFhirResourceDao<Device> getDeviceDao() {
        return OutcomeService.getDeviceDao();
    }

    public static IFhirResourceDao<Task> getTaskDao() {
        return OutcomeService.getTaskDao();
    }

    public static IFhirResourceDao<QuestionnaireResponse> getQrDao() {
        return OutcomeService.getQrDao();
    }

    public static Pattern getTopicPattern() {
        return OutcomeService.getTopicPattern();
    }

    public static EventProcessor getEventProcessor() {
        return OutcomeService.getEventProcessor();
    }

    public static void setEventProcessor(EventProcessor eventProcessor) {
        OutcomeService.setEventProcessor(eventProcessor);
    }

    public static FhirContext getServerFhirContext() {
        return OutcomeService.getServerFhirContext();
    }
}
