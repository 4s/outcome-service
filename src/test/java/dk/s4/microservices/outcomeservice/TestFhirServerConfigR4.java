package dk.s4.microservices.outcomeservice;

import ca.uhn.fhir.jpa.config.BaseJavaConfigR4;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.model.entity.ModelConfig;
import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.dialect.H2Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class TestFhirServerConfigR4 extends BaseJavaConfigR4 {
    @Autowired
    private DataSource myDataSource;
    @Autowired
    @Qualifier("jpaProperties")
    private Properties myJpaProperties;

    public TestFhirServerConfigR4() {
    }

    @Bean
    public DaoConfig daoConfig() {
        DaoConfig retVal = new DaoConfig();
        retVal.setAllowMultipleDelete(true);
        retVal.setAllowExternalReferences(true);
        retVal.setReuseCachedSearchResultsForMillis((Long) null);
        return retVal;
    }

    private String getEnv(String env, String defaultValue) {
        try {
            String value = System.getenv(env);
            return value != null ? value : defaultValue;
        } catch (Exception var4) {
            System.err.println("Error loading environment variable: " + var4);
            return defaultValue;
        }
    }

    @Bean(destroyMethod = "close")
    public DataSource dataSource() {
        BasicDataSource retVal = new BasicDataSource();
        retVal.setDriver(new org.h2.Driver());
        retVal.setUrl("jdbc:h2:mem:testdb_r4;DB_CLOSE_ON_EXIT=FALSE");
        retVal.setMaxWaitMillis(30000);
        retVal.setUsername("");
        retVal.setPassword("");
        retVal.setMaxTotal(1);
        return retVal;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean retVal = super.entityManagerFactory();
        retVal.setPersistenceUnitName("HAPI_PU");
        retVal.setDataSource(this.myDataSource);
        retVal.setJpaProperties(this.myJpaProperties);
        return retVal;
    }

    @Bean
    public Properties jpaProperties() {
        Properties extraProperties = new Properties();
        extraProperties.put("hibernate.format_sql", "false");
        extraProperties.put("hibernate.show_sql", "false");
        extraProperties.put("hibernate.hbm2ddl.auto", "update");
        extraProperties.put("hibernate.dialect", H2Dialect.class.getName());
        extraProperties.put("hibernate.search.model_mapping", ca.uhn.fhir.jpa.search.LuceneSearchMappingFactory.class.getName());
        extraProperties.put("hibernate.search.default.directory_provider", "local-heap");
        extraProperties.put("hibernate.search.lucene_version", "LUCENE_CURRENT");
        extraProperties.put("hibernate.search.autoregister_listeners", "true");
        extraProperties.put("hibernate.temp.use_jdbc_metadata_defaults", "false");
        return extraProperties;
    }

    @Bean
    public ModelConfig modelConfig() {
        return this.daoConfig().getModelConfig();
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager retVal = new JpaTransactionManager();
        retVal.setEntityManagerFactory(entityManagerFactory);
        return retVal;
    }
}