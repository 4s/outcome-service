package dk.s4.microservices.outcomeservice.messaging;

import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.jpa.util.ExpungeOptions;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.outcomeservice.Utils.CustomSearchParameter;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Observation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

/**
 * Class used to delete observations for a given user in a new thread.
 */
public class ObservationDeleter {

    private final static Logger logger = LoggerFactory.getLogger(ObservationDeleter.class);
    private final IFhirResourceDao<Observation> observationDao;
    private final DaoConfig theDaoConfig;
    private final ExpungeOptions expungeOptions;
    private final String OFFICIAL_PATIENT_IDENTIFIER_SYSTEM;
    private final ExecutorService threadPoolExecutor;

    public ObservationDeleter(IFhirResourceDao<Observation> observationDao, DaoConfig theDaoConfig) {
        this.observationDao = observationDao;
        this.theDaoConfig = theDaoConfig;

        this.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM = System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM");
        if (OFFICIAL_PATIENT_IDENTIFIER_SYSTEM == null || OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.isEmpty()) {
            throw new RuntimeException("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM must be set.");
        }

        expungeOptions = new ExpungeOptions()
                .setExpungeDeletedResources(true)
                .setExpungeOldVersions(true);
        threadPoolExecutor = Executors.newSingleThreadExecutor(new CustomizableThreadFactory("delete-observation-"));

    }

    /**
     * Deletes observations for a user given their identifier.
     * The deletion is done in a new thread consecutive calls to the method are queued.
     *
     * @param userId The identifier of the user whose observations are to be deleted.
     */
    public Future<?> deleteObservationsByUserIdentifier(String userId) {

        return threadPoolExecutor.submit(() -> {
            logger.debug("Deleting observations");
            logger.trace("Deleting observations for user " + userId);
            Set<Long> longs = observationDao.searchForIds(new SearchParameterMap
                    (CustomSearchParameter.SUBJECT_IDENTIFIER.getParameter(),
                            new TokenParam(OFFICIAL_PATIENT_IDENTIFIER_SYSTEM, userId)));

            boolean expungeEnabled = theDaoConfig.isExpungeEnabled();
            theDaoConfig.setExpungeEnabled(true);

            for (Long l : longs) {
                IIdType iidType = new IdDt(l);
                observationDao.delete(iidType);
                observationDao.expunge(iidType, expungeOptions);
            }
            logger.debug("Successfully deleted observations");
            logger.trace("Successfully deleted observations for user " + userId);

            theDaoConfig.setExpungeEnabled(expungeEnabled);
        });

    }

}
