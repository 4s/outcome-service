package dk.s4.microservices.outcomeservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dk.s4.microservices.messaging.*;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.fhir.FhirCUDEventProcessor;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.outcomeservice.Utils.CarePlanRetriever;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.UUID;

public class MyEventProcessor extends FhirCUDEventProcessor {

    private final static Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);

    private final IFhirResourceDao<Observation> observationDao;
    private final IFhirResourceDao<Device> deviceDao;
    private final IFhirResourceDao<QuestionnaireResponse> questionnaireResponseDao;
    private final IFhirResourceDao<Task> taskDao;
    private final FhirContext context;
    private final CarePlanRetriever carePlanRetriever;
    private ObservationDeleter observationDeleter;

    private EventProducer eventProducer;

    public MyEventProcessor(FhirContext context,
                            IFhirResourceDao<Observation> observationDao,
                            IFhirResourceDao<Device> deviceDao,
                            IFhirResourceDao<QuestionnaireResponse> questionnaireResponseDao,
                            IFhirResourceDao<Task> taskDao,
                            CarePlanRetriever carePlanRetriever,
                            ObservationDeleter observationDeleter) {
        super(context);
        this.observationDao = observationDao;
        this.deviceDao = deviceDao;
        this.questionnaireResponseDao = questionnaireResponseDao;
        this.taskDao = taskDao;
        this.context = context;
        this.carePlanRetriever = carePlanRetriever;
        this.observationDeleter = observationDeleter;

    }

    public void setEventProducer(EventProducer eventProducer) {
        this.eventProducer = eventProducer;
    }

    @Override
    public boolean processMessage(Topic consumedTopic,
                                  Message receivedMessage,
                                  Topic messageProcessedTopic,
                                  Message outgoingMessage) {
        boolean processingOk = false;

        try {
            logger.trace("Received message: " + receivedMessage.toString());

            messageProcessedTopic
                    .setOperation(Operation.ProcessingFailed)
                    .setDataCategory(consumedTopic.getDataCategory())
                    .setDataType(consumedTopic.getDataType())
                    .setDataCodes(consumedTopic.getDataCodes());

            switch (consumedTopic.getOperation()) {
                case InputReceived:
                    processingOk = processInputReceived(consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage);
                    break;
                case Create:
                case Update:
                    processingOk = super.processMessage(
                            consumedTopic,
                            receivedMessage,
                            messageProcessedTopic,
                            outgoingMessage);
                    break;
                case Delete:

                    // delete if consent has been withdrawn.
                    if (consumedTopic.getDataType().equals("ConsentDeleted")){
                        ObjectNode messageBody = new ObjectMapper().readValue(receivedMessage.getBody(), ObjectNode.class);
                        String userId = messageBody.get("userId").toString().replace("\"", "");

                        deleteObservationsByUserId(userId);

                        messageProcessedTopic.setOperation(Operation.ProcessingOk);
                        OperationOutcome operationOutcome = new OperationOutcome();
                        operationOutcome.addIssue()
                                .setSeverity(OperationOutcome.IssueSeverity.INFORMATION)
                                .setDetails(new CodeableConcept().
                                        setText("Successfully deleted observations for user: " + userId));

                        outgoingMessage.setBodyType("OperationOutcome")
                                .setBodyCategory(Message.BodyCategory.System)
                                .setContentVersion(receivedMessage.getContentVersion())
                                .setSender(System.getenv("SERVICE_NAME"))
                                .setBody(this.context.newJsonParser()
                                        .encodeResourceToString(operationOutcome));

                        processingOk = true;

                    }

                    break;

                default:
                    String errMsg = "Unsupported operation: " + consumedTopic.getOperation();
                    buildErrorMessage(receivedMessage, outgoingMessage, errMsg);
                    logger.error(errMsg);
            }
        } catch (Exception e) {
            buildErrorMessage(receivedMessage, outgoingMessage, e.getMessage());
            logger.error("processMessage - EXCEPTION caught trying to process message: " + e.getMessage(), e);
        }

        return processingOk;
    }

    protected void deleteObservationsByUserId(String userId){
        observationDeleter.deleteObservationsByUserIdentifier(userId);
    }


    @Override
    protected IFhirResourceDao daoForResourceType(String resourceType) {
        if (resourceType.equals("Observation"))
            return observationDao;
        if (resourceType.equals("Device"))
            return deviceDao;
        if (resourceType.equals("QuestionnaireResponse"))
            return questionnaireResponseDao;
        if (resourceType.equals("Task"))
            return taskDao;

        logger.error("No DAO for resource type - should not happen");
        return null;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        Identifier identifier = null;
        if (resource instanceof Observation) {
            Observation observation = (Observation) resource;
            identifier = observation.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(System.getenv("OFFICIAL_OBSERVATION_IDENTIFIER_SYSTEM")))
                    .findAny()
                    .orElse(null);
        } else if (resource instanceof Device) {
            //For devices just require that any kind of identifier is present
            Device device = (Device) resource;
            if (device.getIdentifier().isEmpty())
                return null;
            else
                return device.getIdentifier().get(0);
        } else if (resource instanceof QuestionnaireResponse) {
            QuestionnaireResponse questionnaireResponse = (QuestionnaireResponse) resource;
            if (questionnaireResponse.hasIdentifier() || questionnaireResponse.getIdentifier().hasSystem()) {
                identifier =
                        (questionnaireResponse.getIdentifier().getSystem().equals(
                                System.getenv("OFFICIAL_QUESTIONNAIRE_RESPONSE_IDENTIFIER_SYSTEM")
                        ))
                                ? questionnaireResponse.getIdentifier()
                                : null;
            }
        } else if (resource instanceof Task) {
            Task task = (Task) resource;
            identifier = task.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(System.getenv("OFFICIAL_TASK_IDENTIFIER_SYSTEM")))
                    .findAny()
                    .orElse(null);
        }

        return identifier;
    }


    private boolean processInputReceived(Topic consumedTopic,
                                         Message receivedMessage,
                                         Topic messageProcessedTopic,
                                         Message outgoingMessage) throws Exception {
        boolean processingOk = false;
        switch (consumedTopic.getDataType()) {
            case "Observation":
            case "Device":
            case "QuestionnaireResponse":
                processingOk = handleSingleResourceCreation(consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage);
                break;
            case "Bundle":
                Bundle bundle = context.newJsonParser().parseResource(Bundle.class, receivedMessage.getBody());
                if (!bundle.hasType() || bundle.getType() != Bundle.BundleType.TRANSACTION)
                    buildErrorMessage(receivedMessage, outgoingMessage, "BundleType is not transaction");
                else
                    processingOk = handleTransactionBundle(bundle, consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage);
                break;
            default:
                buildErrorMessage(receivedMessage, outgoingMessage, "InputReceived: Unable to handle data type = " + consumedTopic.getDataType());
        }

        return processingOk;
    }

    private boolean handleTransactionBundle(Bundle bundle,
                                            Topic consumedTopic,
                                            Message receivedMessage,
                                            Topic messageProcessedTopic,
                                            Message outgoingMessage) throws Exception {

        String serverBase = getMyServerBaseAddress();
        IGenericClient client = MyFhirClientFactory.getInstance().getClientFromBaseUrl(serverBase,context);
        client.registerInterceptor(new LoggingInterceptor(true));
        Bundle responseBundle = client.transaction().withBundle(bundle).execute();
        logger.trace("handleTransactionBundle: Bundle response = "
                + context.newJsonParser().encodeResourceToString(responseBundle));

        String notOkStatus = "";
        if (responseBundle.hasType() && responseBundle.getType() == Bundle.BundleType.TRANSACTIONRESPONSE) {
            for (Bundle.BundleEntryComponent entry : responseBundle.getEntry()) {

                if (entry.getResponse().getStatus().equals("200 OK")
                        || entry.getResponse().getStatus().equals("201 Created")) {

                    String entryLocation = entry.getResponse().getLocation();
                    IdType entryId = new IdType(entryLocation);
                    String resourceAsString = "";
                    String resourceType = "";
                    String mdcCode = "";

                    if (entryId.hasResourceType() && entryId.getResourceType().equals("Device")) {
                        Device Device = deviceDao.read(entryId);
                        resourceAsString = context.newJsonParser().encodeResourceToString(Device);
                        resourceType = "Device";
                        mdcCode = getMDCcode(Device);
                    } else if (entryId.hasResourceType() && entryId.getResourceType().equals("Observation")) {
                        Observation observation = observationDao.read(entryId);
                        resourceAsString = context.newJsonParser().encodeResourceToString(observation);
                        resourceType = "Observation";
                        mdcCode = getMDCcode(observation);
                    } else if (entryId.hasResourceType() && entryId.getResourceType().equals("QuestionnaireResponse")) {
                        QuestionnaireResponse questionnaireResponse = questionnaireResponseDao.read(entryId);
                        resourceType = "QuestionnaireResponse";
                        resourceAsString = context.newJsonParser().encodeResourceToString(questionnaireResponse);
                        if (entry.getResponse().getStatus().equals("201 Created")) {
                            createTask(questionnaireResponse, receivedMessage);
                        }

                    } else
                        logger.warn("Should not happen: Bundle response with unknown resource type: {}", entryId.getResourceType());

                    if (!resourceAsString.isEmpty()) {
                        Topic.Operation event = Operation.DataCreated; //"201 Created" status
                        if (entry.getResponse().getStatus().equals("200 OK"))
                            event = Operation.DataUpdated;

                        Topic topic = new Topic()
                                .setOperation(event)
                                .setDataCategory(Category.FHIR)
                                .setDataType(entryId.getResourceType());
                        if (!mdcCode.isEmpty())
                            topic.addDataCode(mdcCode);

                        Message message = new Message()
                                .setSender(System.getenv("SERVICE_NAME"))
                                .setBody(resourceAsString)
                                .setCorrelationId(receivedMessage.getCorrelationId())
                                .setTransactionId(receivedMessage.getTransactionId())
                                .setSecurity(receivedMessage.getSecurity())
                                .setContentVersion(System.getenv("FHIR_VERSION"))
                                .setBodyCategory(BodyCategory.FHIR)
                                .setBodyType(resourceType)
                                .setLocation(entryLocation)
                                .setSession(receivedMessage.getSession())
                                .setEtag(entry.getResponse().getEtag());

                        eventProducer.sendMessage(topic, message);

                        String eventString = "";
                        if (entry.getResponse().getStatus().equals("200 OK"))
                            eventString = "updated";
                        else if (entry.getResponse().getStatus().equals("201 Created"))
                            eventString = "created";

                        logger.debug("processMessage - Successfully {} FHIR Resource (received from Kafka) in the " +
                                "database - entryId = {}", eventString, entryId);
                    }
                } else
                    notOkStatus = entry.getResponse().getStatus();
            }
            if (!notOkStatus.isEmpty()) {
                buildErrorMessage(receivedMessage, outgoingMessage,
                        "Status of entry in transaction-response Bundle neither \"200 OK\" nor \"201 Created\" : " + notOkStatus);
                return false;
            } else {
                messageProcessedTopic
                        .setOperation(Operation.TransactionCompleted)
                        .setDataCategory(consumedTopic.getDataCategory())
                        .setDataType(consumedTopic.getDataType())
                        .setDataCodes(consumedTopic.getDataCodes());
                outgoingMessage.cloneFields(receivedMessage);
                outgoingMessage.setSender(System.getenv("SERVICE_NAME"));
                outgoingMessage.setBody(context.newJsonParser().encodeResourceToString(responseBundle));
                logger.debug("processMessage - Successfully handled transaction bundle: "
                        + context.newJsonParser().encodeResourceToString(responseBundle));
                return true;
            }
        } else {
            buildErrorMessage(receivedMessage, outgoingMessage, "BundleType returned from transaction is not transaction-response");
            return false;
        }
    }

    private boolean handleSingleResourceCreation(Topic consumedTopic,
                                                 Message receivedMessage,
                                                 Topic messageProcessedTopic,
                                                 Message outgoingMessage) throws MessageIncompatibleException, UserContextResolverInterface.UserContextResolverException {
        DaoMethodOutcome outcome = null;

        if (consumedTopic.getDataType().equals("Observation")) {
            Observation observation = context.newJsonParser().parseResource(Observation.class, receivedMessage.getBody());
            outcome = observationDao.create(observation);
        } else if (consumedTopic.getDataType().equals("Device")) {
            Device device = context.newJsonParser().parseResource(Device.class, receivedMessage.getBody());
            outcome = deviceDao.create(device);
        } else if (consumedTopic.getDataType().equals("QuestionnaireResponse")) {
            QuestionnaireResponse questionnaireResponse = context.newJsonParser().parseResource(QuestionnaireResponse.class, receivedMessage.getBody());
            outcome = questionnaireResponseDao.create(questionnaireResponse);
            if (outcome != null && outcome.getCreated()) {
                createTask(questionnaireResponse, receivedMessage);
            }

        }

        if (outcome != null && outcome.getCreated()) {
            messageProcessedTopic
                    .setOperation(Operation.DataCreated)
                    .setDataCategory(consumedTopic.getDataCategory())
                    .setDataType(consumedTopic.getDataType())
                    .setDataCodes(consumedTopic.getDataCodes());
            outgoingMessage.cloneFields(receivedMessage);
            outgoingMessage
                    .setSender(System.getenv("SERVICE_NAME"))
                    .setLocation(outcome.getId().getValue())
                    .setBody(context.newJsonParser().encodeResourceToString(outcome.getOperationOutcome()));
            logger.debug("processMessage - Successfully stored FHIR Resource (received from Kafka) in the database: "
                    + outcome.getOperationOutcome());
            return true;
        } else if (outcome != null) {
            buildErrorMessage(receivedMessage, outgoingMessage,context.newJsonParser().encodeResourceToString(outcome.getOperationOutcome()));
            return false;
        } else {
            buildErrorMessage(receivedMessage, outgoingMessage, "Unknow error trying to create resource in database");
            return false;
        }
    }

    @Override
    protected void buildErrorMessage(Message receivedMessage, Message outgoingMessage, String msg) {
        EventOperationOutcome eventOperationOutcome = new EventOperationOutcome(EventOperationOutcome.ERROR, msg);
        outgoingMessage
                .setBodyType(receivedMessage.getBodyType())
                .setContentVersion(receivedMessage.getContentVersion())
                .setSender(System.getenv("SERVICE_NAME"))
                .setBody(eventOperationOutcome.toString());
        logger.error(msg);
    }

    private String getMyServerBaseAddress() throws Exception {
        String serverBaseUrl = System.getenv("SERVER_BASE_URL");
        if (serverBaseUrl != null && !serverBaseUrl.isEmpty())
            return serverBaseUrl;
        else
            throw new Exception("SERVER_BASE_URL is undefined - required for transacting bundles");
    }

    private String getMDCcode(Observation observation) {
        for (Coding coding : observation.getCode().getCoding()) {
            if (coding.getSystem().equalsIgnoreCase(System.getenv("REQUIRED_CODING_SYSTEM")))
                return "MDC" + coding.getCode();
        }
        return "";
    }

    private String getMDCcode(Device Device) {
        if (Device.hasType() && Device.getType().hasCoding()) {
            for (Coding coding : Device.getType().getCoding()) {
                if (coding.getSystem().equalsIgnoreCase(System.getenv("REQUIRED_CODING_SYSTEM")))
                    return "MDC" + coding.getCode();
            }
        }
        return "";
    }

    /**
     * Creates a FHIR task with the code "acknowledgement" and the status "requested" based on the resource provided
     *
     * @param resource        The resource
     * @param receivedMessage The message received from kafka
     */
    void createTask(Resource resource, Message receivedMessage) throws UserContextResolverInterface.UserContextResolverException {
        String resourceAsString = context.newJsonParser().encodeResourceToString(constructTask(resource,receivedMessage));

        Topic topic = FhirTopics.create("Task");
        postToKafka(receivedMessage, resourceAsString, topic);
    }

    Task constructTask(Resource resource, Message receivedMessage) throws UserContextResolverInterface.UserContextResolverException {
        Task task = new Task();
        task.addIdentifier(
                new Identifier()
                        .setSystem(System.getenv("UUID_CODING_SYSTEM"))
                        .setValue(UUID.randomUUID().toString())
        ).setStatus(
                Task.TaskStatus.REQUESTED
        ).setIntent(
                Task.TaskIntent.ORDER
        ).setCode(
                new CodeableConcept()
                        .addCoding(
                                new Coding()
                                        .setSystem(System.getenv("TASK_TYPE_CODING_SYSTEM"))
                                        .setCode("acknowledgement")
                                        .setDisplay("Acknowledgement")
                        )
                        .setText("Acknowledgement")
        ).setAuthoredOn(
                new Date()
        );

        switch (resource.getResourceType()) {
            case QuestionnaireResponse:
                QuestionnaireResponse qr = (QuestionnaireResponse) resource;
                if (qr.hasBasedOn() && qr.getBasedOn().size() > 0) {
                    Reference basedOn = qr.getBasedOn().get(0);
                    task.addBasedOn(basedOn);
                    task.setOwner(carePlanRetriever.getCarePlanAuthorFromBasedOn(basedOn.getIdentifier()));
                }

                if (qr.hasIdentifier()) {
                    task.setFocus(
                            new Reference()
                                    .setIdentifier(qr.getIdentifier())
                                    .setType("QuestionnaireResponse")
                    );
                }

                if (qr.hasSubject()) {
                    task.setFor(qr.getSubject());
                }
                break;
            case Observation:
                Observation obs = (Observation) resource;
                if (obs.hasBasedOn() && obs.getBasedOn().size() > 0) {
                    Reference basedOn = obs.getBasedOn().get(0);
                    task.addBasedOn(basedOn);
                    task.setOwner(carePlanRetriever.getCarePlanAuthorFromBasedOn(basedOn.getIdentifier()));
                }

                if (obs.hasIdentifier()) {
                    task.setFocus(
                            new Reference()
                                    .setIdentifier(obs.getIdentifierFirstRep())
                                    .setType("Observation")
                    );
                }

                if (obs.hasSubject()) {
                    task.setFor(obs.getSubject());
                }
                break;
        }
        return task;
    }

    void postToKafka(Message receivedMessage, String resourceAsString, Topic topic) {
        Message message = new Message()
                .setSender(System.getenv("SERVICE_NAME"))
                .setBody(resourceAsString)
                .setCorrelationId(receivedMessage.getCorrelationId())
                .setTransactionId(receivedMessage.getTransactionId())
                .setSecurity(receivedMessage.getSecurity())
                .setContentVersion(System.getenv("FHIR_VERSION"))
                .setSession(receivedMessage.getSession())
                .setBodyCategory(BodyCategory.FHIR)
                .setBodyType("Task");

        logger.trace("New kafkaMessage created: \n topic: {} \n message: {}", topic.toString(), message.toString());

        eventProducer.sendMessage(topic, message);

    }

    /**
     * For testing, internal method, use observation deleter setup instead.
     */
    @Deprecated()
    protected ObservationDeleter getObservationDeleter() {
        if (this.observationDeleter != null) {
            return observationDeleter;
        }
        throw new RuntimeException("Binary Deleter not initialized");
    }

    /**
     * For testing, internal method, use observation deleter setup instead.
     */
    @Deprecated
    protected void setObservationDeleter(ObservationDeleter observationDeleter) {
        this.observationDeleter = observationDeleter;
    }
}