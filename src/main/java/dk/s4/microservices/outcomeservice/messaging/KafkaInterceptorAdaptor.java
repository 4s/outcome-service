package dk.s4.microservices.outcomeservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import dk.s4.microservices.messaging.EventConsumerInterceptorAdaptor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.TopicDataCode;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Task;

public class KafkaInterceptorAdaptor extends EventConsumerInterceptorAdaptor {

    private final FhirContext context;

    public KafkaInterceptorAdaptor(FhirContext context){
        this.context = context;
    }

    @Override
    public boolean incomingMessagePreProcessMessage(Topic consumedTopic, Message receivedMessage) {
        if(consumedTopic.getDataType().equals("Task")){
            IBaseResource resource = context.newJsonParser().parseResource(receivedMessage.getBody());
            TopicDataCode dataCode = TopicDataCode.from((Task) resource);
            return dataCode == TopicDataCode.QUESTIONNAIRE_RESPONSE || dataCode == TopicDataCode.OBSERVATION;
        }
        return true;
    }
}
