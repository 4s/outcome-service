package dk.s4.microservices.outcomeservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.search.reindex.IResourceReindexingSvc;
import ca.uhn.fhir.jpa.searchparam.registry.ISearchParamRegistry;
import ca.uhn.fhir.parser.StrictErrorHandler;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import dk.s4.microservices.genericresourceservice.servlet.JpaServerGenericServlet;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.Env;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.MetricsEventConsumerInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.fhir.SearchParameterFacade;
import dk.s4.microservices.microservicecommon.security.*;
import dk.s4.microservices.outcomeservice.Utils.CarePlanRetriever;
import dk.s4.microservices.outcomeservice.health.HealthEndpoint;
import dk.s4.microservices.outcomeservice.messaging.KafkaInterceptorAdaptor;
import dk.s4.microservices.outcomeservice.messaging.MyEventProcessor;
import dk.s4.microservices.outcomeservice.messaging.ObservationDeleter;
import dk.s4.microservices.outcomeservice.provider.FHIRDeviceResourceProvider;
import dk.s4.microservices.outcomeservice.provider.FHIRObservationResourceProvider;
import dk.s4.microservices.outcomeservice.provider.FHIRQuestionnaireResponseResourceProvider;
import dk.s4.microservices.outcomeservice.provider.FHIRTaskResourceProvider;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static dk.s4.microservices.microservicecommon.fhir.ResourceUtil.getResourceStream;

/**
 * Observation Service.
 * <p>
 * It exposes a RESTful interface for accessing FHIR Observation
 * resources. The POST/PUT operations of the REST interface should not be used
 * directly. Instead, new resources are created when receiving messages from Kafka:
 * <p>
 * This service listens on Kafka for incoming FHIR Observation messages,
 * and stores the FHIR Observation resources in the database.
 */
public class OutcomeService extends JpaServerGenericServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(OutcomeService.class);
    private static FhirContext fhirContext;
    private static EventProcessor eventProcessor;
    private static ObservationDeleter observationDeleter;
    private static Pattern topicPattern;
    private static IFhirResourceDao<Observation> observationDao;
    private static IFhirResourceDao<Device> deviceDao;
    private static IFhirResourceDao<QuestionnaireResponse> qrDao;
    private static IFhirResourceDao<Task> taskDao;
    private static IResourceReindexingSvc reindexing;
    private static DaoConfig daoConfig;
    private UserContextResolverInterface userResolver;
    private KafkaConsumeAndProcess kafkaConsumeAndProcess;
    private Thread kafkaConsumeAndProcessThread;
    private CarePlanRetriever carePlanRetriever;

    public OutcomeService() {
        super(getMyFhirContext(), logger);
    }

    /**
     * Singleton FhirContext
     *
     * @return the FhirContext
     */
    private static FhirContext getMyFhirContext() {
        if (fhirContext == null) {
            fhirContext = FhirContext.forR4();
            fhirContext.setParserErrorHandler(new StrictErrorHandler());
            fhirContext.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
            fhirContext.getRestfulClientFactory().setSocketTimeout(1200 * 1000);
        }
        return fhirContext;
    }

    @Override
    public UserContextResolverInterface getUserResolver() {
        if(userResolver == null) {
            if (System.getenv("ENABLE_DIAS_AUTHENTICATION").equals("true")) {
                userResolver = new DiasUserContextResolver(System.getenv("USER_CONTEXT_SERVICE_URL"));
            } else if (System.getenv("ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION").equals("true")) {
                userResolver = new KeycloakGatekeeperUserContextResolver();
            } else if (System.getenv("ENABLE_OAUTH2_PROXY_AUTHORIZATION").equals("true")) {
                userResolver = new OAuth2ProxyUserContextResolver();
            } else {
                userResolver = new DefaultUserContextResolver();
            }
        }
        return userResolver;
    }

    @Override
    public void destroy() {
        System.out.println("Shutting down outcome-service");
        if (kafkaConsumeAndProcess != null) {
            kafkaConsumeAndProcess.stopThread();
        }
        if (kafkaConsumeAndProcessThread != null) {
            try {
                kafkaConsumeAndProcessThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void registerAndCheckEnvironmentVars() {
        Env.registerRequiredEnvVars(Arrays.asList(
                "SERVICE_NAME",
                "FHIR_VERSION",
                "CORRELATION_ID",
                "TRANSACTION_ID",
                "LOG_LEVEL",
                "LOG_LEVEL_DK_S4",
                "ENABLE_KAFKA",
                "SERVER_BASE_URL",
                "DATABASE_URL",
                "DATABASE_USERNAME",
                "DATABASE_PASSWORD",
                "OFFICIAL_OBSERVATION_IDENTIFIER_SYSTEM",
                "OFFICIAL_QUESTIONNAIRE_RESPONSE_IDENTIFIER_SYSTEM",
                "OFFICIAL_TASK_IDENTIFIER_SYSTEM"
        ));
        Env.registerConditionalEnvVars("ENABLE_KAFKA",
                Arrays.asList(
                        "KAFKA_BOOTSTRAP_SERVER",
                        "KAFKA_KEY_DESERIALIZER",
                        "KAFKA_VALUE_DESERIALIZER",
                        "KAFKA_ENABLE_AUTO_COMMIT",
                        "KAFKA_AUTO_COMMIT_INTERVAL_MS",
                        "KAFKA_SESSION_TIMEOUT_MS",
                        "KAFKA_GROUP_ID",
                        "KAFKA_ACKS",
                        "KAFKA_RETRIES",
                        "KAFKA_KEY_SERIALIZER",
                        "KAFKA_VALUE_SERIALIZER",
                        "ENABLE_HEALTH",
                        "HEALTH_FILE_PATH",
                        "HEALTH_INTERVAL_MS")
        );
    }


    @Override
    protected void initialize() throws ServletException {
        super.initialize();
        registerAndCheckEnvironmentVars();
        logger.info("initialize");

        // Get the spring context from the web container (it's declared in web.xml)
        WebApplicationContext myAppCtx = ContextLoaderListener.getCurrentWebApplicationContext();

        observationDao = myAppCtx.getBean("myObservationDaoR4", IFhirResourceDao.class);

        deviceDao = myAppCtx.getBean("myDeviceDaoR4", IFhirResourceDao.class);
        qrDao = myAppCtx.getBean("myQuestionnaireResponseDaoR4", IFhirResourceDao.class);
        taskDao = myAppCtx.getBean("myTaskDaoR4", IFhirResourceDao.class);
        IFhirResourceDao<SearchParameter> searchParameterDaoR4 = myAppCtx.getBean("mySearchParameterDaoR4", IFhirResourceDao.class);
        reindexing = myAppCtx.getBean(IResourceReindexingSvc.class);

        daoConfig = myAppCtx.getBean(DaoConfig.class);

        //Initiate Custom SearchParameters
        SearchParameterFacade searchParameterFacade = new SearchParameterFacade(searchParameterDaoR4,getMyFhirContext());
        try {
            searchParameterFacade.installSearchParameter(getResourceStream("Observation_BasedOnIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("Observation_SubjectIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("Observation_DerivedFromIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("QR_BasedOnIdentifierSearchParameter.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("QR_SubjectIdentifierSearchParameter.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("Task_ForIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("Task_OwnerIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("Task_BasedOnIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("Task_ReasonCodeSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("Task_FocusIdentifierSearchParam.json"));
            myAppCtx.getBean(ISearchParamRegistry.class).forceRefresh();
        } catch (IOException e) {
            throw new InternalError("Something went wrong while reading the custom SearchParameters");
        }

        List<IResourceProvider> resourceProviders = new ArrayList<>();
        resourceProviders.add(new FHIRObservationResourceProvider(taskDao, qrDao, observationDao, getMyFhirContext()));
        resourceProviders.add(new FHIRDeviceResourceProvider(deviceDao, getMyFhirContext()));
        resourceProviders.add(new FHIRQuestionnaireResponseResourceProvider(qrDao, taskDao, getMyFhirContext()));
        resourceProviders.add(new FHIRTaskResourceProvider(taskDao, getMyFhirContext()));
        setResourceProviders(resourceProviders);

        if (!kafkaEnabled())
            logger.warn("Kafka messaging is NOT enabled");

        //Subscribe to InputReceived_FHIR_Bundle(_MDC.*)|InputReceived_FHIR_Observation(_MDC.*)|Create_FHIR_Device
        //(init topics outside enableKafka clause to accommodate test class init flow)
        String topicPatternString = getTopicPatternString();
        logger.info("Subscribing to pattern: " + topicPatternString);
        topicPattern = Pattern.compile(topicPatternString);

        if (kafkaEnabled()) {
            logger.info("Initializing Kafka");
            MyFhirClientFactory clientFactory = MyFhirClientFactory.getInstance();
            carePlanRetriever = new CarePlanRetriever(System.getenv("PATIENTCARE_SERVICE_URL"), getMyFhirContext(),clientFactory);

            observationDeleter = new ObservationDeleter(observationDao,daoConfig);

            eventProcessor = new MyEventProcessor(getMyFhirContext(), observationDao, deviceDao, qrDao, taskDao,
                    carePlanRetriever, observationDeleter);


            try {
                KafkaEventProducer kafkaEventProducer = new KafkaEventProducer(System.getenv("SERVICE_NAME"));
                ((MyEventProcessor) eventProcessor).setEventProducer(kafkaEventProducer);

                kafkaConsumeAndProcess = new KafkaConsumeAndProcess(topicPattern, kafkaEventProducer, eventProcessor);
                kafkaConsumeAndProcess.registerInterceptor(new MetricsEventConsumerInterceptorAdaptor());
                kafkaConsumeAndProcess.registerInterceptor(new KafkaInterceptorAdaptor(getMyFhirContext()));
                if (Env.isSetToTrue("ENABLE_DIAS_AUTHENTICATION")) {
                    kafkaConsumeAndProcess.registerInterceptor(new DiasEventConsumerInterceptor());
                }
                kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);
                kafkaConsumeAndProcessThread.start();
                HealthEndpoint.registerKafkaConsumeAndProcess(kafkaConsumeAndProcess);
            } catch (KafkaInitializationException | MessagingInitializationException e) {
                logger.error("Error during Kafka initialization: ", e);
            }
        }
        else {
            logger.warn("Kafka disabled");
        }
    }

    static String getTopicPatternString() {
        String topicPatternString = System.getenv("SERVICE_INPUT_TOPIC_PATTERN_STRING");
        if( topicPatternString != null && !topicPatternString.isEmpty()) return topicPatternString;
        else return new Topic()
                .setOperation(Operation.InputReceived)
                .setDataCategory(Category.FHIR).toString() +
                Topic.separator + "Bundle(" + Topic.separator + ".*)*|"
                + new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR).toString() +
                Topic.separator + "Observation(" + Topic.separator + ".*)*|"
                + FhirTopics.create("QuestionnaireResponse").toString() + "|"
                + FhirTopics.create("Device").toString() + "|"
                + FhirTopics.create("Task").toString() + "|"
                + new Topic()
                .setOperation(Operation.Update)
                .setDataCategory(Category.FHIR).toString() +
                Topic.separator + "Observation(" + Topic.separator + ".*)*|"
                + FhirTopics.update("QuestionnaireResponse").toString() + "|"
                + FhirTopics.update("Device").toString() + "|"
                + FhirTopics.update("Task").toString() + "|"
                + new Topic()
                .setOperation(Operation.Delete)
                .setDataType("ConsentDeleted")
                .setDataCategory(Category.System);
    }

    private boolean kafkaEnabled() {
        String enableKafkaString = System.getenv("ENABLE_KAFKA");
        return (enableKafkaString != null && enableKafkaString.equals("true"));
    }

    /**
     * For testing
     */
    static void forceReindexing() {
        if (reindexing != null) {
            reindexing.forceReindexingPass();
        }
        else {
            logger.error("Reindexing failed");
        }
    }

    /**
     * For testing
     */
    static IFhirResourceDao<Observation> getObservationDao() {
        if (observationDao != null) {
            return observationDao;
        }
        throw new RuntimeException("Observation dao not initialized");
    }

    /**
     * For testing
     */
    static DaoConfig getDaoConfig() {
        if (daoConfig != null) {
            return daoConfig ;
        }
        throw new RuntimeException("Dao config not initialized");
    }

    /**
     * For testing
     */
    static IFhirResourceDao<Device> getDeviceDao() {
        if (deviceDao != null) {
            return deviceDao;
        }
        throw new RuntimeException("Device dao not initialized");
    }

    /**
     * For testing
     */
    static IFhirResourceDao<Task> getTaskDao() {
        if (taskDao != null) {
            return taskDao;
        }
        throw new RuntimeException("Task dao not initialized");
    }

    /**
     * For testing
     */
    static IFhirResourceDao<QuestionnaireResponse> getQrDao() {
        if (qrDao != null) {
            return qrDao;
        }
        throw new RuntimeException("qr dao not initialized");
    }

    /**
     * For testing
     */
    static Pattern getTopicPattern() {
        if (topicPattern != null) {
            return topicPattern;
        }
        throw new RuntimeException("Topics not initialized");
    }

    /**
     * For testing
     */
    static EventProcessor getEventProcessor() {
        if (eventProcessor != null) {
            return eventProcessor;
        }
        throw new RuntimeException("Event processor not initialized");
    }

    /**
     * For testing
     */
    static void setEventProcessor(EventProcessor eventProcessor) {
        OutcomeService.eventProcessor = eventProcessor;
    }

    /**
     * For testing
     */
    static FhirContext getServerFhirContext() {
        return getMyFhirContext();
    }
}
