package dk.s4.microservices.outcomeservice.health;

import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.json.simple.JSONObject;

/**
 * Exposes web service endpoint for checking overall health of the service.
 */
@Path("/")
public class HealthEndpoint {

	static private KafkaConsumeAndProcess kafkaConsumeAndProcess;

	static public void registerKafkaConsumeAndProcess(KafkaConsumeAndProcess kcap) {
		kafkaConsumeAndProcess = kcap;
	}
	/**
	 * Web service endpoint for checking overall health of the service.
	 *
	 * @return response containing health status
	 */
	@GET
	@Path("/")
	public Response getMsg() {
		if (System.getenv("ENABLE_KAFKA").equals("true")) {
			if (kafkaConsumeAndProcess == null || !kafkaConsumeAndProcess.isHealthy()) {
				ResponseBuilder builder = Response.serverError();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Kafka consumer is not healthy");
				builder.entity(jsonObject.toJSONString());
				return builder.build();
			}
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", "all good");
		return Response.ok(jsonObject.toJSONString(), MediaType.APPLICATION_JSON).build();
	}
}