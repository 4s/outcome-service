package dk.s4.microservices.outcomeservice.Utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.microservicecommon.fhir.CarePlanVerifier;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarePlanRetriever {

    private static final Logger logger = LoggerFactory.getLogger(CarePlanVerifier.class);

    private String serviceBaseUrl;
    private FhirContext fhirContext;
    private MyFhirClientFactory clientFactory;

    public CarePlanRetriever(String serviceBaseUrl,
                             FhirContext fhirContext,
                             MyFhirClientFactory clientFactory) {
        this.serviceBaseUrl = serviceBaseUrl;
        this.fhirContext = fhirContext;
        this.clientFactory = clientFactory;
    }

    public Reference getCarePlanAuthorFromBasedOn(Identifier basedOn) {
        CarePlan carePlan = getCarePlanFromBasedOn(basedOn);
        return carePlan.getAuthor();
    }

    private CarePlan getCarePlanFromBasedOn(Identifier basedOn){
        logger.debug("getCarePlanFromBasedOn");
        try {
            IGenericClient patientCareClient = clientFactory.getClientFromBaseUrl(serviceBaseUrl, fhirContext);
            String searchUrl = patientCareClient.getServerBase() +
                    "/CarePlan?identifier=" +
                    new TokenParam().setValue(basedOn.getValue()).setSystem(basedOn.getSystem())
                            .getValueAsQueryToken(fhirContext);
            Bundle bundle = patientCareClient.search()
                    .byUrl(searchUrl).returnBundle(Bundle.class).execute();
            if (!bundle.hasEntry() || bundle.getEntry().isEmpty()){
                throw ExceptionUtils.fatalInternalErrorException("The carePlan which the questionnaireResponse/observation is basedOn could not be found");
            }
            if(bundle.getEntry().size() > 1){
                throw ExceptionUtils.fatalInternalErrorException("More than one carePlan matches the identifier in the basedOn field of the questionnaireResponse/observation");
            }
            if(!((CarePlan) bundle.getEntryFirstRep().getResource()).getStatus().equals(CarePlan.CarePlanStatus.ACTIVE)){
                throw ExceptionUtils.fatalInternalErrorException("The carePlan which the questionnaireResponse/observation is basedOn is not active");
            }
            return (CarePlan) bundle.getEntryFirstRep().getResource();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }
}
