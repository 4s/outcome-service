package dk.s4.microservices.outcomeservice.Utils;

import ca.uhn.fhir.rest.api.server.IBundleProvider;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BundleFilter<T extends IBaseResource> {
    private final Predicate<T> predicate;

    public BundleFilter(Predicate<T> predicate) {
        this.predicate = predicate;
    }

    public List<T> apply(IBundleProvider provider) {
        return provider.getResources(0, provider.size()).stream().map(t -> (T) t).filter(predicate).collect(Collectors.toList());
    }
}
