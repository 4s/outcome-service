package dk.s4.microservices.outcomeservice.Utils;

import org.hl7.fhir.r4.model.Task;

import java.util.function.Predicate;

public class TaskBundleFilter extends BundleFilter<Task> {
    private static final Predicate<Task> hasFocusAndIdentifier = task -> task.hasFocus() && task.getFocus().hasIdentifier() && task.getFocus().hasType();
    private static final Predicate<Task> isObservation = task -> task.getFocus().getType().equals("Observation");
    private static final Predicate<Task> taskFocusIdentifierHasValue = task -> task.getFocus().getIdentifier().hasValue();

    public TaskBundleFilter() {
        super(task -> hasFocusAndIdentifier.test(task) && isObservation.test(task) && taskFocusIdentifierHasValue.test(task));
    }
}