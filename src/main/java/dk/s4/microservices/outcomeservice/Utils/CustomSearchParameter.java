package dk.s4.microservices.outcomeservice.Utils;

public enum CustomSearchParameter {
    PATIENT_IDENTIFIER("patient-identifier"),
    OWNER_IDENTIFIER("owner-identifier"),
    BASED_ON_IDENTIFIER("basedOn-identifier"),
    SUBJECT_IDENTIFIER("subject-identifier"),
    FOCUS_IDENTIFIER("focus-identifier"),
    DERIVED_FROM_IDENTIFIER("derivedFrom-identifier"),
    REASON_CODE("reason-code");

    private String parameter;

    CustomSearchParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }
}
