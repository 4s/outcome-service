package dk.s4.microservices.outcomeservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.outcomeservice.Utils.CustomSearchParameter;
import dk.s4.microservices.outcomeservice.servlet.OutcomeService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR QuestionnaireResponse resource,
 * which supports the operations: read, create, update, and search.
 * 
 */
public class FHIRQuestionnaireResponseResourceProvider extends BaseJpaResourceProvider<QuestionnaireResponse> {

    private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResponseResourceProvider.class);
    private IFhirResourceDao<Task> taskDao;

    public FHIRQuestionnaireResponseResourceProvider(IFhirResourceDao<QuestionnaireResponse> qrDao, IFhirResourceDao<Task> taskDao, FhirContext fhirContext) {
        super(qrDao);
        this.taskDao = taskDao;
        setContext(fhirContext);
    }

	/**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<QuestionnaireResponse> getResourceType() {
		return QuestionnaireResponse.class;
	}

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
            RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * Search for {@link QuestionnaireResponse}s with a given subject-identifier.
     * Optionally refine search with a date-range.
     *
     * @param request the request
     * @param response the response
     * @param requestDetails the details
     * @param subjectIdentifier the identifier of the subject
     * @param basedOn identifier of the CarePlan to which the QuestionnaireResponse relates
     * @param date the date range
     * @return search results contained in an IBundleProvider
     */
    @Search(queryName = "searchBySubjectIdentifier")
    public IBundleProvider searchBySubjectIdentifier(HttpServletRequest request, HttpServletResponse response, RequestDetails requestDetails,
                                                     @RequiredParam(name = "subject-identifier") TokenParam subjectIdentifier,
                                                     @OptionalParam(name = QuestionnaireResponse.SP_BASED_ON) TokenParam basedOn,
                                                     @OptionalParam(name = QuestionnaireResponse.SP_AUTHORED) DateRangeParam date) {
        SearchParameterMap searchParameterMap = new SearchParameterMap();
        searchParameterMap.add(CustomSearchParameter.SUBJECT_IDENTIFIER.getParameter(), subjectIdentifier);
        searchParameterMap.add(CustomSearchParameter.BASED_ON_IDENTIFIER.getParameter(), basedOn);
        searchParameterMap.add(QuestionnaireResponse.SP_AUTHORED, date);
        return getDao().search(searchParameterMap, requestDetails, response);
    }

    /**
     * Search for {@link QuestionnaireResponse}s with a given basedOn-identifier.
     * Optionally refine search with a date-range.
     *
     * @param request the request
     * @param response the response
     * @param requestDetails the details
     * @param basedOnIdentifier identifier of the CarePlan to which the QuestionnaireResponse relates
     * @param date the date range
     * @return search results contained in an IBundleProvider
     */
    @Search(queryName = "searchByBasedOnIdentifier")
    public IBundleProvider searchByBasedOnIdentifier(HttpServletRequest request, HttpServletResponse response, RequestDetails requestDetails,
                                                     @RequiredParam(name = QuestionnaireResponse.SP_BASED_ON) TokenParam basedOnIdentifier,
                                                     @OptionalParam(name = QuestionnaireResponse.SP_AUTHORED) DateRangeParam date) {
        SearchParameterMap searchParameterMap = new SearchParameterMap();
        searchParameterMap.add(CustomSearchParameter.BASED_ON_IDENTIFIER.getParameter(), basedOnIdentifier);
        searchParameterMap.add(QuestionnaireResponse.SP_AUTHORED, date);
        return getDao().search(searchParameterMap, requestDetails, response);
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public QuestionnaireResponse read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * Returns bundle of QuestionnaireResponses that are the focus of tasks with the code and the status provided as
     * arguments.
     *
     * Example invocation:
     * http://fhir.example.com/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus?code=acknowledgement&amp;status=requested
     *
     * @param code The code value of the Tasks resources to search for (required)
     * @param status The status of the Tasks resources to search for(required)
     *
     * @return A bundle containing all QuestionnaireResponses that match the query.
     */

    /**
     * Returns bundle of QuestionnaireResponses that are the focus of tasks with the code and the status provided as
     * arguments.
     *
     * Example invocation:
     * http://fhir.example.com/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus?code=acknowledgement&amp;status=requested
     *
     * @param request the request
     * @param response the response
     * @param requestDetails the details
     * @param code The code value of the Tasks resources to search for (required)
     * @param status The status of the Tasks resources to search for(required)
     * @param owner the owner of the Task resources to search for(required)
     * @return A bundle containing all QuestionnaireResponses that match the query.
     */
    @Search(queryName = "getFocusForTasksWithCodeAndStatus")
    public Bundle getFocusForTasksWithCodeAndStatus(
            HttpServletRequest request,
            HttpServletResponse response,
            RequestDetails requestDetails,
            @RequiredParam(name=Task.SP_CODE) TokenParam code,
            @RequiredParam(name=Task.SP_STATUS) TokenParam status,
            @OptionalParam(name=Task.SP_OWNER) TokenOrListParam owner
    ) {
        Bundle retVal = new Bundle();
        try {
            this.startRequest(request);
            logger.debug("QuestionnaireResponse - getFocusForTasksWithCodeAndStatus");

            // retrieve tasks with specified code and status
            SearchParameterMap taskParamMap = new SearchParameterMap();
            taskParamMap.add(Task.SP_CODE, code);
            taskParamMap.add(Task.SP_STATUS, status);
            taskParamMap.add(CustomSearchParameter.OWNER_IDENTIFIER.getParameter(), owner);
            IBundleProvider taskBundleProvider = this.taskDao.search(taskParamMap, requestDetails, response);

            // return empty bundle if no tasks matching the search parameters exist
            if (taskBundleProvider.size() == 0) {
                return retVal;
            }

            // create new search with for questionnaire responses that are the focus of the tasks
            SearchParameterMap qrParamMap = new SearchParameterMap();
            TokenOrListParam qrSearchParam = new TokenOrListParam();
            List<IBaseResource> tasks = taskBundleProvider.getResources(0, taskBundleProvider.size());
            for(int i = 0; i < tasks.size(); i++) {
                Task task = (Task) tasks.get(i);

                // retrieve identifier from task focus if one exists
                Identifier qrIdentifier = null;
                if (task.hasFocus() && task.getFocus().hasIdentifier()) {
                    if (task.getFocus().hasType() && !task.getFocus().getType().equals("QuestionnaireResponse")) {
                        continue;
                    }
                    qrIdentifier = task.getFocus().getIdentifier();

                }

                // skip if identifier is malformed
                if (qrIdentifier == null || !qrIdentifier.hasSystem() || !qrIdentifier.hasValue()) {
                    continue;
                }

                // add identifier system and value to search parameters
                qrSearchParam.addOr(new TokenParam(
                        task.getFocus().getIdentifier().getSystem(),
                        task.getFocus().getIdentifier().getValue()
                ));
            }
            qrParamMap.add(QuestionnaireResponse.SP_IDENTIFIER, qrSearchParam);

            // retrieve questionnaires identified by the tasks
            IBundleProvider qrBundleProvider = this.getDao().search(qrParamMap, requestDetails, response);

            // return empty bundle if no questionnaire responses matching the search parameters exist
            if (qrBundleProvider.size() == 0) {
                return retVal;
            }

            // populate bundle
            List<IBaseResource> qrs = qrBundleProvider.getResources(0, qrBundleProvider.size());
            for(int i = 0; i < qrs.size(); i++) {
                retVal.addEntry().setResource((QuestionnaireResponse) qrs.get(i));
            }
        } finally {
            this.endRequest(request);
        }
        return retVal;
    }
}
