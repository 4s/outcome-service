package dk.s4.microservices.outcomeservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Patch;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.outcomeservice.Utils.CustomSearchParameter;
import dk.s4.microservices.outcomeservice.Utils.TaskBundleFilter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.QuestionnaireResponse.QuestionnaireResponseItemAnswerComponent;
import org.hl7.fhir.r4.model.QuestionnaireResponse.QuestionnaireResponseItemComponent;
import org.hl7.fhir.r4.model.Task;
import org.hl7.fhir.r4.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR Observation resource, which supports the
 * operations: read, create, update, and search.
 */
public class FHIRObservationResourceProvider extends BaseJpaResourceProvider<Observation> {

    private static final Logger logger =
        LoggerFactory.getLogger(FHIRObservationResourceProvider.class);
    private final IFhirResourceDao<Task> taskDao;
    private final TaskBundleFilter observationFilter;
    private final IFhirResourceDao<QuestionnaireResponse> qrDao;



    public FHIRObservationResourceProvider(IFhirResourceDao<Task> taskDao,
        IFhirResourceDao<QuestionnaireResponse> qrDao, IFhirResourceDao<Observation> dao,
        FhirContext fhirContext) {
        this(taskDao, qrDao, dao, fhirContext, new TaskBundleFilter());
    }

    protected FHIRObservationResourceProvider(IFhirResourceDao<Task> taskDao,
        IFhirResourceDao<QuestionnaireResponse> qrDao, IFhirResourceDao<Observation> dao,
        FhirContext fhirContext, TaskBundleFilter filter) {
        super(dao);
        this.taskDao = taskDao;
        this.qrDao = qrDao;
        this.observationFilter = filter;
        setContext(fhirContext);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Observation> getResourceType() {
        return Observation.class;
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in
     * BaseJpaResourceProvider and always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Observation read(HttpServletRequest theRequest, @IdParam IIdType theId,
        RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException(
            "RESTful read operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in
     * BaseJpaResourceProvider and always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
        RequestDetails theRequestDetails, @ResourceParam String theBody,
        PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException(
            "RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * Search for {@link QuestionnaireResponse}s with a given basedOn-identifier. Optionally refine
     * search with a date-range.
     *
     * @param request           the request
     * @param response          the response
     * @param requestDetails    the details
     * @param basedOnIdentifier identifier of the CarePlan to which the QuestionnaireResponse
     *                          relates
     * @return search results contained in an IBundleProvider
     */
    @Search(queryName = "searchByBasedOnIdentifier")
    public IBundleProvider searchByBasedOnIdentifier(HttpServletRequest request,
        HttpServletResponse response, RequestDetails requestDetails,
        @RequiredParam(name = Observation.SP_BASED_ON) TokenParam basedOnIdentifier) {
        SearchParameterMap searchParameterMap = new SearchParameterMap();
        searchParameterMap
            .add(CustomSearchParameter.BASED_ON_IDENTIFIER.getParameter(), basedOnIdentifier);
        return getDao().search(searchParameterMap, requestDetails, response);
    }

    /**
     * Search for {@link Observation}s with a given subject-identifier. Optionally refine search
     * with a code and date-range.
     *
     * @param request           the request
     * @param response          the response
     * @param requestDetails    the details
     * @param subjectIdentifier the identifier of the subject
     * @param date              the date range
     * @param code              the code
     * @param basedOn           careplan identifier to which the observation relates
     * @param derivedFrom       identifier of resource that the observation is derived from
     * @return search result in a IBundleProvider
     */
    @Search(queryName = "searchBySubjectIdentifier")
    public IBundleProvider searchBySubjectIdentifier(HttpServletRequest request,
        HttpServletResponse response, RequestDetails requestDetails,
        @RequiredParam(name = "subject-identifier") TokenParam subjectIdentifier,
        @OptionalParam(name = Observation.SP_DATE) DateRangeParam date,
        @OptionalParam(name = Observation.SP_CODE) TokenParam code,
        @OptionalParam(name = Observation.SP_BASED_ON) TokenParam basedOn,
        @OptionalParam(name = Observation.SP_DERIVED_FROM) TokenParam derivedFrom) {
        SearchParameterMap searchParameterMap = new SearchParameterMap();
        searchParameterMap
            .add(CustomSearchParameter.SUBJECT_IDENTIFIER.getParameter(), subjectIdentifier);
        searchParameterMap.add(Observation.SP_DATE, date);
        searchParameterMap.add(Observation.SP_CODE, code);
        searchParameterMap.add(CustomSearchParameter.BASED_ON_IDENTIFIER.getParameter(), basedOn);
        searchParameterMap
            .add(CustomSearchParameter.DERIVED_FROM_IDENTIFIER.getParameter(), derivedFrom);
        return getDao().search(searchParameterMap, requestDetails, response);
    }

    /**
     * Returns the Observation with the given identifier.
     *
     * @param request        the request.
     * @param response       the response.
     * @param requestDetails the request details.
     * @param identifier     the identifier of the observation.
     * @return an IBundleProvider containing any observation matching the given identifier
     */

    @Search(queryName = "getByIdentifier")
    public IBundleProvider getByIdentifier(HttpServletRequest request, HttpServletResponse response,
        RequestDetails requestDetails,
        @RequiredParam(name = Observation.SP_IDENTIFIER) TokenParam identifier) {
        SearchParameterMap searchParameterMap = new SearchParameterMap();
        searchParameterMap.add(Observation.SP_IDENTIFIER, identifier);
        return getDao().search(searchParameterMap, requestDetails, response);
    }

    /**
     * Returns bundle of Observations that are the focus of tasks with the code and the status
     * provided as arguments.
     * <p>
     * Example invocation: http://fhir.example.com/Observation?_query=getFocusForTasksWithCodeAndStatus?code=acknowledgement&amp;status=requested
     *
     * @param code   The code value of the Tasks resources to search for (required)
     * @param status The status of the Tasks resources to search for(required)
     * @return A bundle containing all Observations that match the query.
     */
    @Search(queryName = "getFocusForTasksWithCodeAndStatus")
    public Bundle getFocusForTasksWithCodeAndStatus(HttpServletRequest request,
        HttpServletResponse response, RequestDetails requestDetails,
        @RequiredParam(name = Task.SP_CODE) TokenParam code,
        @RequiredParam(name = Task.SP_STATUS) TokenParam status,
        @OptionalParam(name = Task.SP_OWNER) TokenOrListParam owner) {
        Bundle retVal = new Bundle();
        try {
            this.startRequest(request);
            logger.debug("Observation - getFocusForTasksWithCodeAndStatus");

            // retrieve tasks with specified code and status
            SearchParameterMap taskParamMap = new SearchParameterMap();
            taskParamMap.add(CustomSearchParameter.OWNER_IDENTIFIER.getParameter(), owner);
            taskParamMap.add(Task.SP_CODE, code);
            taskParamMap.add(Task.SP_STATUS, status);
            IBundleProvider taskBundleProvider =
                this.taskDao.search(taskParamMap, requestDetails, response);

            // return empty bundle if no tasks matching the search parameters exist
            if (taskBundleProvider.size() == 0) {
                return retVal;
            }
            TokenOrListParam obsSearchParam = new TokenOrListParam();
            for (Task task : observationFilter.apply(taskBundleProvider)) {
                Identifier identifier = task.getFocus().getIdentifier();
                obsSearchParam.add(identifier.getSystem(), identifier.getValue());
            }

            if (obsSearchParam.getValuesAsQueryTokens().isEmpty()) {
                return retVal;
            }

            SearchParameterMap searchParameterMap = new SearchParameterMap();
            searchParameterMap.add(Observation.SP_IDENTIFIER, obsSearchParam);

            // retrieve observations identified by the tasks
            IBundleProvider obsBundleProvider =
                this.getDao().search(searchParameterMap, requestDetails, response);

            // return empty bundle if no observations matching the search parameters exist
            if (obsBundleProvider.size() == 0) {
                return retVal;
            }

            // populate bundle
            List<IBaseResource> qrs = obsBundleProvider.getResources(0, obsBundleProvider.size());
            for (int i = 0; i < qrs.size(); i++) {
                retVal.addEntry().setResource((Observation) qrs.get(i));
            }
        } finally {
            this.endRequest(request);
        }
        return retVal;
    }

    /**
     * Returns bundle of Observations that are referenced in any questionnaires that are the focus
     * of acknowledgement tasks with the status and owner provided as arguments.
     * <p>
     * Example invocation: http://fhir.example.com/Observation?_query=getObservationsForAcknowledgement?status=requested&amp;owner=urn:oid:1.2.208.176.1.1|272101000016000
     *
     * @param status The status of the Tasks resources to search for(required)
     * @param owner  The organization that owns the Task resources
     * @return A bundle containing all Observations that match the query.
     */
    @Search(queryName = "getObservationsForAcknowledgement")
    public Bundle getObservationsForAcknowledgement(HttpServletRequest request,
        HttpServletResponse response, RequestDetails requestDetails,
        @RequiredParam(name = Task.SP_STATUS) TokenParam status,
        @OptionalParam(name = Task.SP_OWNER) TokenOrListParam owner) {
        Bundle returnBundle = new Bundle();
        try {
            // retrieve tasks with specified code and status
            SearchParameterMap taskParamMap = new SearchParameterMap();
            taskParamMap.add(Task.SP_CODE, new TokenParam().setValue("acknowledgement"));
            taskParamMap.add(Task.SP_STATUS, status);
            taskParamMap.add(CustomSearchParameter.OWNER_IDENTIFIER.getParameter(), owner);
            IBundleProvider taskBundleProvider =
                this.taskDao.search(taskParamMap, requestDetails, response);

            // return empty bundle if no tasks matching the search parameters exist
            if (taskBundleProvider.size() == 0) {
                return returnBundle;
            }

            // create new search with for questionnaire responses that are the focus of the tasks
            SearchParameterMap qrParamMap = new SearchParameterMap();
            TokenOrListParam qrSearchParam = new TokenOrListParam();
            List<IBaseResource> tasks =
                taskBundleProvider.getResources(0, taskBundleProvider.size());
            for (IBaseResource iBaseResource : tasks) {
                Task task = (Task) iBaseResource;

                // retrieve identifier from task focus if one exists
                Identifier qrIdentifier = null;
                if (task.hasFocus() && task.getFocus().hasIdentifier()) {
                    if (task.getFocus().hasType() && !task.getFocus().getType()
                        .equals("QuestionnaireResponse")) {
                        continue;
                    }
                    qrIdentifier = task.getFocus().getIdentifier();

                }

                // skip if identifier is malformed
                if (qrIdentifier == null || !qrIdentifier.hasSystem() || !qrIdentifier.hasValue()) {
                    continue;
                }

                // add identifier system and value to search parameters
                qrSearchParam.addOr(new TokenParam(task.getFocus().getIdentifier().getSystem(),
                    task.getFocus().getIdentifier().getValue()));
            }
            qrParamMap.add(QuestionnaireResponse.SP_IDENTIFIER, qrSearchParam);

            // retrieve questionnaires identified by the tasks
            IBundleProvider qrBundleProvider =
                this.qrDao.search(qrParamMap, requestDetails, response);

            // return empty bundle if no questionnaire responses matching the search parameters exist
            if (qrBundleProvider.size() == 0) {
                return returnBundle;
            }

            List<Identifier> extractedObservationIdentifiers = extractObservationReferences(
                qrBundleProvider.getResources(0, qrBundleProvider.size()));
            // create new search with for questionnaire responses that are the focus of the tasks
            SearchParameterMap obsParamMap = new SearchParameterMap();
            TokenOrListParam obsSearchParam = new TokenOrListParam();
            for (Identifier obsIdentifier : extractedObservationIdentifiers) {
                if (obsIdentifier == null || !obsIdentifier.hasSystem() || !obsIdentifier
                    .hasValue()) {
                    continue;
                }
                // add identifier system and value to search parameters
                obsSearchParam
                    .addOr(new TokenParam(obsIdentifier.getSystem(), obsIdentifier.getValue()));
            }

            if (obsSearchParam.getValuesAsQueryTokens().isEmpty()) {
                return returnBundle;
            }

            obsParamMap.add(Observation.SP_IDENTIFIER, obsSearchParam);

            IBundleProvider obsBundleProvider =
                this.getDao().search(obsParamMap, requestDetails, response);
            // return empty bundle if no observations matching the search parameters exist
            if (obsBundleProvider.size() == 0) {
                return returnBundle;
            }

            // populate bundle
            List<IBaseResource> observations =
                obsBundleProvider.getResources(0, obsBundleProvider.size());
            for (IBaseResource obs : observations) {
                returnBundle.addEntry().setResource((Observation) obs);
            }

        } finally {
            this.endRequest(request);
        }
        return returnBundle;
    }


    protected List<Identifier> extractObservationReferences(List<IBaseResource> qrs) {
        List<Identifier> observationIdentifiers = new ArrayList<>();
        for (IBaseResource qrResource : qrs) {
            QuestionnaireResponse qr = (QuestionnaireResponse) qrResource;
            for (QuestionnaireResponseItemComponent item : qr.getItem()) {
                observationIdentifiers.addAll(extractObservationReferencesFromItem(item));
            }
        }
        return observationIdentifiers;
    }

    protected List<Identifier> extractObservationReferencesFromItem(
        QuestionnaireResponseItemComponent item) {
        List<Identifier> returnList = new ArrayList<>();
        if (item.hasItem()) {
            for (QuestionnaireResponseItemComponent containedItem : item.getItem()) {
                returnList.addAll(extractObservationReferencesFromItem(containedItem));
            }
        }
        for (QuestionnaireResponseItemAnswerComponent answer : item.getAnswer()) {
            if (answer.getValue() instanceof Quantity) {
                Extension linkedObservationExtension = answer
                    .getExtensionByUrl(System.getenv("OFFICIAL_LINKED_OBSERVATION_EXTENSION_URL"));
                if (linkedObservationExtension != null && linkedObservationExtension.hasValue()) {
                    Type value = linkedObservationExtension.getValue();
                    if (value instanceof Reference) {
                        returnList.add(((Reference) value).getIdentifier());
                    }
                }
            }
        }
        return returnList;
    }
}
