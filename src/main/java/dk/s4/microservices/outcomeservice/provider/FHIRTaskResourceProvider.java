package dk.s4.microservices.outcomeservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.SimpleBundleProvider;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.outcomeservice.Utils.CustomSearchParameter;
import org.hl7.fhir.instance.model.api.IBaseReference;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a resource provider for the FHIR Task resource,
 * which supports the operations: read, create, update, and search.
 * 
 */
public class FHIRTaskResourceProvider extends BaseJpaResourceProvider<Task> {

    private static final Logger logger = LoggerFactory.getLogger(FHIRTaskResourceProvider.class);

    public FHIRTaskResourceProvider(IFhirResourceDao<Task> dao, FhirContext fhirContext) {
        super(dao);
        setContext(fhirContext);
    }

	/**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<Task> getResourceType() {
		return Task.class;
	}

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
            RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Task read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * Returns a Bundle of tasks that match the search criteria given
     * @param theRequest the request
     * @param theResponse the response
     * @param theRequestDetail the details
     * @param status the status of the task
     * @param code the code of the task
     * @param focus the identifier of the focus of the task
     * @param subject the patient subject of the task
     * @param owner the organization that owns the task
     * @param basedOn the CarePlan upon which the task is based on
     * @param reasonCode the reason code of the task
     * @return Bundle containing tasks
     */
    @Search(allowUnknownParams=true)
    public IBundleProvider genericSearch(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                         RequestDetails theRequestDetail,
                                         @OptionalParam(name=Task.SP_STATUS) TokenOrListParam status,
                                         @OptionalParam(name=Task.SP_CODE) TokenParam code,
                                         @OptionalParam(name=Task.SP_FOCUS) TokenParam focus,
                                         @OptionalParam(name=Task.SP_SUBJECT) TokenParam subject,
                                         @OptionalParam(name=Task.SP_OWNER) TokenOrListParam owner,
                                         @OptionalParam(name=Task.SP_BASED_ON) TokenParam basedOn,
                                         @OptionalParam(name="reason-code") TokenParam reasonCode) {
        logger.debug("search");
        SearchParameterMap parameterMap = new SearchParameterMap();
        parameterMap.add(CustomSearchParameter.PATIENT_IDENTIFIER.getParameter(), subject);
        parameterMap.add(CustomSearchParameter.OWNER_IDENTIFIER.getParameter(), owner);
        parameterMap.add(CustomSearchParameter.REASON_CODE.getParameter(), reasonCode);
        parameterMap.add(CustomSearchParameter.BASED_ON_IDENTIFIER.getParameter(), basedOn);
        parameterMap.add(CustomSearchParameter.FOCUS_IDENTIFIER.getParameter(), focus);
        parameterMap.add(Task.SP_CODE, code);
        parameterMap.add(Task.SP_STATUS, status);

        return getDao().search(parameterMap,theRequestDetail,theResponse);
    }

    /**
     * Returns a bundle of tasks for a given patient, given code and within a given period (optional).
     *
     * @param theRequest the http request
     * @param theResponse the http response
     * @param theRequestDetail the http request details
     * @param code the FHIR Task code, either acknowledgement, review or activity
     * @param identifier the identifier of the patient
     * @param authored_on the period
     * @return Bundle containing tasks
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task+type+coding+system">Task type coding system</a>
     */

    @Search(queryName = "getTaskForPatientWithCode")
    public IBundleProvider getTaskForPatientWithCode(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                                     RequestDetails theRequestDetail,
                                                     @RequiredParam(name=Task.SP_CODE) TokenParam code,
                                                     @RequiredParam(name= Patient.SP_IDENTIFIER) TokenParam identifier,
                                                     @OptionalParam(name = Task.SP_OWNER) TokenOrListParam owner,
                                                     @OptionalParam(name=Task.SP_AUTHORED_ON) DateRangeParam authored_on){
        logger.debug("getTaskForPatientWithCode");

        SearchParameterMap parameterMap = new SearchParameterMap();
        parameterMap.add(CustomSearchParameter.PATIENT_IDENTIFIER.getParameter(), identifier);
        parameterMap.add(CustomSearchParameter.OWNER_IDENTIFIER.getParameter(), owner);
        parameterMap.add(Task.SP_CODE, code);
        parameterMap.add(Task.SP_AUTHORED_ON, authored_on);

        return getDao().search(parameterMap,theRequestDetail,theResponse);
    }

    /**
     * Returns a bundle of tasks with a given code and focus (optional).
     *
     * @param theRequest the http request
     * @param theResponse the http response
     * @param theRequestDetail the http request details
     * @param code the FHIR Task code, either acknowledgement, review or activity
     * @param owner the owner of the task
     * @param focusType the focus of the task
     * @param authored_on the period
     * @return Bundle containing tasks
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task+type+coding+system">Task type coding system</a>
     */

    @Search(queryName = "getTaskWithFocusTypeAndCode")
    public IBundleProvider getTaskWithFocusAndCode(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                                     RequestDetails theRequestDetail,
                                                     @RequiredParam(name=Task.SP_CODE) TokenParam code,
                                                     @OptionalParam(name=Task.SP_OWNER) TokenOrListParam owner,
                                                     @OptionalParam(name="focusType") TokenParam focusType,
                                                    @OptionalParam(name=Task.SP_AUTHORED_ON) DateRangeParam authored_on){
        logger.debug("getTaskForPatientWithCode");

        SearchParameterMap parameterMap = new SearchParameterMap();
        parameterMap.add(CustomSearchParameter.OWNER_IDENTIFIER.getParameter(), owner);
        parameterMap.add(Task.SP_CODE, code);
        parameterMap.add(Task.SP_AUTHORED_ON, authored_on);

        IBundleProvider iBundleProvider = getDao().search(parameterMap,theRequestDetail,theResponse);
        if (iBundleProvider.size() == 0){
            String exceptionString = "No Tasks match the given search criteria: " + "code=" + code.getValue();
            if (authored_on != null) {
                exceptionString = exceptionString + ", authoredOn=" + authored_on.toString();
            }
            throw new ResourceNotFoundException(exceptionString);
        }
        if (focusType == null){
            return iBundleProvider;
        } else {
            List<IBaseResource> list = new ArrayList<>();
            for (IBaseResource resource : iBundleProvider.getResources(0,iBundleProvider.size())){
                Task task = (Task) resource;
                if(task.getFocus().getType() == null){
                    continue;
                }
                if (task.getFocus().getType().equals(focusType.getValue())){
                    list.add(task);
                }
            }
            return new SimpleBundleProvider(list);
        }
    }

}
