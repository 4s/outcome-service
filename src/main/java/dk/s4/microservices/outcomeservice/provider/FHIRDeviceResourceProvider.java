package dk.s4.microservices.outcomeservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Patch;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR Device resource,
 * which supports the operations: read, create, update, and search.
 * 
 */
public class FHIRDeviceResourceProvider extends BaseJpaResourceProvider<Device> {
	
    private static final Logger logger = LoggerFactory.getLogger(FHIRDeviceResourceProvider.class);

    public FHIRDeviceResourceProvider(IFhirResourceDao<Device> dao, FhirContext fhirContext) {
        super(dao);
        setContext(fhirContext);
    }

	/**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<Device> getResourceType() {
		return Device.class;
	}

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
            RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Device read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * Returns the device with the given identifier.
     * @param request the request.
     * @param response the response.
     * @param requestDetails the request details.
     * @param identifier the identifier of the device.
     * @return an IBundleProvider containing any Device resource matching the given identifier
     */

    @Search(queryName = "getByIdentifier")
    public IBundleProvider getByIdentifier(HttpServletRequest request, HttpServletResponse response, RequestDetails requestDetails,
                                           @RequiredParam(name = Device.SP_IDENTIFIER) TokenParam identifier){
        SearchParameterMap searchParameterMap = new SearchParameterMap();
        searchParameterMap.add(Device.SP_IDENTIFIER, identifier);
        return getDao().search(searchParameterMap, requestDetails, response);
    }
}
